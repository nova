/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libastrodb/astrodb.h>
#include "star.h"

static struct astrodb_schema_object star_fields[] = {
	sky2k4v_schema
};

/* we only accept the --prefix as our 1 arg*/
int main (int argc, char* argv[])
{ 
	struct astrodb_library *lib;
	struct astrodb_db *db;
	struct astrodb_table *table;
	
	if (argc != 2) {
		printf ("usage: %s prefix\n", argv[0]);
		return -1;
	}
	
	printf("%s using libastrodb %s\n", argv[0], astrodb_get_version());
	printf("\n\n** Downloading 25Mb data. This may take some time.\n"); 
	
	/* set the remote db and initialise local repository/cache */
	lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", argv[1]);
	if (lib == NULL) {
		printf("failed to open library\n");
		return -1;
	}
	
	/* create a dbalog */
	db = astrodb_create_db(lib, "V", "109", 
			0.0, 360.0, -90.0, 90.0, 15.0, -2.0, 0);
	if (db == NULL) {
		printf("failed to create db\n");
		return -1;
	}
		
	table = astrodb_table_create(db, "sky2kv4", ADB_MEM | ADB_FILE );
	if (table == NULL) {
		printf("failed to create table\n");
		return -1;
	}
			
	if (astrodb_table_register_schema(table, star_fields, 
		astrodb_size(star_fields), sizeof(struct sky2kv4_object)) < 0)
		g_critical("%s: failed to register object type\n", __func__);
	
	/* Vmag is blank in some records in the dataset, so we use can Vder
	 * as an alternate field.
	 */
	if (astrodb_table_alt_column(table, "Vmag", "Vder", 0))
		printf("warning: failed to add alt index\n");
	
	/* We want to quickly search the dataset based on object ID and HD number */
	if (astrodb_table_hash_key(table, "ID"))
		g_warning("%s: failed to hash on ID\n", __func__);
	if (astrodb_table_hash_key(table, "HD"))
		g_warning("%s: failed to hash on HD\n", __func__);		
			
	/* Import the dataset from remote/local repo into memory/disk cache */   
	if (astrodb_table_open(table, 20, 10, 20) < 0) {
		printf("failed to open table\n");
		return -1;
	}
	
	/* were done with the dataset */
	astrodb_table_close(table);
	
	/* were now done with dbalog */
	astrodb_db_free(db);
	
	astrodb_close_library(lib);
	return 0;
}
