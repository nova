/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "db/db.h"
#include "sky/sky.h"
#include "sky/legend.h"
#include "ui/glade.h"

static struct ui_config nova_ui_config = {
	.ui_width	= 600,
	.ui_height	= 600,
};

static gboolean menu_help_clicked_event(GtkWidget *widget,
						gpointer user_data)
{
	printf("about\n");
	return TRUE;
}

static GtkWidget *create_file_menu(GtkWidget *gtk_sky)
{
	GtkWidget *file_menu;
	GtkWidget *open_item;
	GtkWidget *save_item;
	GtkWidget *quit_item;
	
	file_menu = gtk_menu_new ();

	/* Create the menu items */
	open_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_OPEN, NULL);
	save_item = gtk_menu_item_new_with_label ("Save");
	quit_item = gtk_menu_item_new_with_label ("Quit");

	/* Add them to the menu */
	gtk_menu_shell_append (GTK_MENU_SHELL (file_menu), open_item);
	gtk_menu_shell_append (GTK_MENU_SHELL (file_menu), save_item);
	gtk_menu_shell_append (GTK_MENU_SHELL (file_menu), quit_item);

	/* Attach the callback functions to the activate signal */
	g_signal_connect_swapped (G_OBJECT (open_item), "activate",
                          G_CALLBACK (menu_help_clicked_event),
                          (gpointer) gtk_sky);
	g_signal_connect_swapped (G_OBJECT (save_item), "activate",
                          G_CALLBACK (menu_help_clicked_event),
                          (gpointer) gtk_sky);

	/* We can attach the Quit menu item to our exit function */
	g_signal_connect_swapped (G_OBJECT (quit_item), "activate",
                              G_CALLBACK (gtk_main_quit),
                              (gpointer) "file.quit");

	return file_menu;
}

static GtkWidget *create_view_menu(GtkWidget *gtk_sky)
{
	GtkWidget *view_menu;
	GtkWidget *markers_item;
	
	view_menu = gtk_menu_new ();

	/* Create the menu items */
	markers_item = gtk_menu_item_new_with_label ("Markers...");

	/* Add them to the menu */
	gtk_menu_shell_append (GTK_MENU_SHELL (view_menu), markers_item);

	/* Attach the callback functions to the activate signal */
	g_signal_connect (G_OBJECT (markers_item), "activate",
                          G_CALLBACK(on_sky_markers_activate),
                          (gpointer) gtk_sky);
	return view_menu;
}

static GtkWidget *create_help_menu(GtkWidget *gtk_sky)
{
	GtkWidget *help_menu;
	GtkWidget *about_item;
	
	help_menu = gtk_menu_new ();

	/* Create the menu items */
	about_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_ABOUT, NULL);

	/* Add them to the menu */
	gtk_menu_shell_append (GTK_MENU_SHELL (help_menu), about_item);

	/* Attach the callback functions to the activate signal */
	g_signal_connect_swapped (G_OBJECT (about_item), "activate",
                          G_CALLBACK(on_sky_markers_activate),
                          (gpointer) gtk_sky);
	return help_menu;
}

static GtkWidget *create_menubar(GtkWidget *gtk_sky)
{
	GtkWidget *menu_bar;
	GtkWidget *file_item;
	GtkWidget *view_item;
	GtkWidget *help_item;
	
	menu_bar = gtk_menu_bar_new ();
	file_item = gtk_menu_item_new_with_label ("File");
	view_item = gtk_menu_item_new_with_label ("View");
	help_item = gtk_menu_item_new_with_label ("Help");

	gtk_menu_item_set_submenu (GTK_MENU_ITEM (file_item), 
					create_file_menu(gtk_sky));
	gtk_menu_bar_append (GTK_MENU_BAR (menu_bar), file_item);
	
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (view_item), 
					create_view_menu(gtk_sky));
	gtk_menu_bar_append (GTK_MENU_BAR (menu_bar), view_item);
	
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (help_item), 
					create_help_menu(gtk_sky));
	gtk_menu_bar_append (GTK_MENU_BAR (menu_bar), help_item);
	gtk_menu_item_right_justify(GTK_MENU_ITEM(help_item));
	
	return menu_bar;
}

static gboolean tool_button_clicked_event(GtkWidget *widget,
						gpointer user_data)
{
	printf("convert\n");
	return TRUE;
}

static GtkWidget *create_toolbar(GtkWidget *gtk_sky)
{
	GtkWidget *toolbar;
	GtkToolItem *item;
	
	toolbar = gtk_toolbar_new();
	gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
	
	item = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_CONVERT);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), item, 0);
	g_signal_connect(item, "clicked",
			G_CALLBACK(tool_button_clicked_event), gtk_sky);
	
	return toolbar;
}

int main (int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *gtk_sky;
	GtkWidget *gtk_legend;
	GtkWidget *vbox;
	GtkWidget *menubar;
	GtkWidget *toolbar;
	int err;
	
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	gtk_init (&argc, &argv);
	
	err = ui_gconf_get_context(&nova_ui_config);
	if (err < 0) {
		g_warning("%s: no gconf config - creating default\n", __func__);
		ui_gconf_set_context(&nova_ui_config);
	}
	
	err = db_init();
	if (err < 0) {
		g_critical("db_init: failed %d\n", err);
		return err;
	}

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Nova");
	g_signal_connect (window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	
	gtk_sky = virtual_sky_new();
	gtk_widget_set_usize(gtk_sky, nova_ui_config.ui_width, 
				nova_ui_config.ui_height);
	
	gtk_legend = legend_new();
	gtk_widget_set_usize(gtk_legend, -1, 20);
	
	menubar = create_menubar(gtk_sky);
	toolbar = create_toolbar(gtk_sky);
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	gtk_box_pack_start (GTK_BOX (vbox), menubar, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox),
                     gtk_sky, 
                     TRUE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(vbox),
                     gtk_legend, 
                     FALSE, FALSE, 0);

	err = ui_init();
	if (err < 0) {
		g_critical("%s: failed to init UI\n", __func__);
		return err;
	}

	gtk_widget_show_all(window);
	sky_connect_signals(gtk_sky);
	sky_set_legend(gtk_sky, gtk_legend);

	gtk_main ();
	
	db_release();
	return 0;
}
