/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include "db.h"
#include "star.h"

#define CAT_HOME	NOVA_CATALOG_DIR

struct table_desc {
	gchar *cat_name;
	gchar *class;
	gchar *num;
	gchar* table_name;
	struct astro_object *object;
	struct astrodb_db *cat;
	struct astrodb_table *table;
};

struct astro_object star_table = {
	.type	=	"star",
	.init	=	star_object_init,
	.free	=	star_object_free,
	.render =	star_object_render,
};

/* default datasets */
static struct table_desc dlib[] = {
	{"Sky2000", "V", "109", "sky2kv4", &star_table},
};

static struct table_desc olib[16];

static struct astrodb_library *nova_lib;

gint db_init(void)
{
	gint i;
	
	nova_lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", 
		CAT_HOME);
	if (nova_lib == NULL)
		g_critical("failed to open astro library at %s\n", CAT_HOME);

	for (i = 0; i < astrodb_size(dlib); i++) {

    		/* ra,dec,mag bounds are set here along with the 3D 
		 * tile array size */
		dlib[i].cat = astrodb_create_db(nova_lib, dlib[i].class, 
						dlib[i].num, 
						0.0, 360.0, 
						-90.0, 90.0, 
						18.0, -2.0, 0);
    		if (dlib[i].cat == NULL)
    			g_critical("failed to create db for class %s num %s\n",
    				dlib[i].class, dlib[i].num);
    		
		/* use the first dataset in this example */
		dlib[i].table = astrodb_table_create(dlib[i].cat, 
							dlib[i].table_name, 
							ADB_MEM | ADB_FILE);
    		if (dlib[i].table == NULL) 
    			g_critical("failed to create table %s\n", 
    				dlib[i].table_name);

 		if (dlib[i].object->init)
    			dlib[i].object->init(dlib[i].table);
	}
	return 0;
}

void db_release(void)
{
	gint i;
	
	for (i = 0; i < astrodb_size(dlib); i++) {
		if (dlib[i].object->free)
			dlib[i].object->free(dlib[i].table);
		astrodb_db_free(dlib[i].cat);
	}
	astrodb_close_library(nova_lib);
}

struct astrodb_table *db_get_table(gchar *name)
{
	gint i;
	
	for (i = 0; i < astrodb_size(olib); i++) {
		if (!strncmp(name, olib[i].table_name, strlen(name)))
			return olib[i].table;
	}
	
	return NULL;
}

struct astrodb_table *db_get_default_table(gchar *type)
{
	gint i;

	for (i = 0; i < astrodb_size(dlib); i++) {
		if (!strncmp(type, dlib[i].object->type, strlen(type)))
			return dlib[i].table;
	}
	
	return NULL;
}
