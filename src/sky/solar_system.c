/*
 * Copyright (C) 2005 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include "solar_system.h"
#include "solar_object.h"
#include "planet.h"
#include "lunar.h"
#include "sol.h"
#include "tile.h"

#define LABEL_FONT_SIZE		15.0

void planets_render(Sky *sky)
{
	struct render_object *rc = &sky->robject;
	struct projection *proj = &sky->projection; 
	struct render_flags *rm = &sky->solar_system_settings.planets;
	struct tile_array *ta = sky->tile;
			
	/* planet label fonts */	 
	cairo_select_font_face (rc->cr, "Sans", CAIRO_FONT_SLANT_NORMAL, 
				CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (rc->cr, LABEL_FONT_SIZE);
	
	mercury.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		mercury.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
		
	venus.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		venus.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
		
	mars.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		mars.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
	
	jupiter.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		jupiter.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
	
	saturn.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		saturn.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
	
	uranus.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		uranus.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
	
	neptune.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		neptune.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
	
	pluto.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		pluto.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
}

void solar_render(Sky *sky)
{
	struct render_object *rc = &sky->robject;
	struct projection *proj = &sky->projection; 
	struct render_flags *rm = &sky->solar_system_settings.solar;
	struct tile_array *ta = sky->tile;
	
	/* planet label fonts */	 
	cairo_select_font_face (rc->cr, "Sans", CAIRO_FONT_SLANT_NORMAL, 
				CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (rc->cr, LABEL_FONT_SIZE);
	
	solar.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		solar.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
}

void lunar_render(Sky *sky)
{	
	struct render_object *rc = &sky->robject;
	struct projection *proj = &sky->projection; 
	struct render_flags *rm = &sky->solar_system_settings.lunar;
	struct tile_array *ta = sky->tile;
	
	/* planet label fonts */	 
	cairo_select_font_face (rc->cr, "Sans", CAIRO_FONT_SLANT_NORMAL, 
				CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (rc->cr, LABEL_FONT_SIZE);
	 
	lunar.update_cache(rc);
	proj->trans->sky_to_proj_equ(proj, &rc->coord[0]);
	
	if (projection_is_visible0(proj, rc)) {
		lunar.render(rc, rm);
		if (rc->type == RT_FULL)
			tile_add_object(ta, rc);
	}
}

