/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#define _GNU_SOURCE		/* for NAN */

#include <math.h>
#include <time.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libnova/utility.h>
#include <libnova/transform.h>
#include <libnova/julian_day.h>
#include <libnova/ln_types.h>
#include <libastrodb/astrodb.h>
#include "sky.h"
#include "render.h"
#include "star.h"
#include "tile.h"
#include "db.h"
#include "legend.h"
#include "constellation.h"
#include "grid.h"
#include "solar_system.h"
#include "glade.h"

#define NOVA_SKY_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((obj), NOVA_TYPE_SKY, Sky))

G_DEFINE_TYPE (GtkSky, virtual_sky, GTK_TYPE_DRAWING_AREA);

#define SKY_MOVE_SIZE		15
#define SKY_UPDATE_MAG_DIFF	1
#define SKY_MAG_SCALER		8
#define SKY_ZOOM_OUT		1.1
#define SKY_ZOOM_IN		0.9
#define SKY_MAX_RA		360.0

static gboolean virtual_sky_expose (GtkWidget *sky_widget, GdkEventExpose *event);
static gboolean virtual_sky_update (gpointer data);
static GdkCursor *sky_cursor_arrow;
static GdkCursor *sky_cursor_ptr;

static inline void sky_get_posn(Sky *sky, struct render_coord *coord)
{
	sky->projection.trans->proj_to_sky_equ(&sky->projection, coord);
}

static inline struct astrodb_object* 
	virtual_sky_get_object (Sky *sky, struct render_coord* rc)
{
	return tile_get_object_at(sky->tile, rc);
}

static gboolean sky_complete_scroll_render(gpointer data)
{
	GtkWidget *widget = data;
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	
	/* redraw still pending ? */
	if (sky->redraw_pending)
		return TRUE;
	
	sky->robject.type = RT_FULL;
	gtk_widget_queue_draw(widget);
	return FALSE;
}

static void sky_move_abs_posn(Sky *sky, gfloat ra, gfloat dec)
{
	sky->projection.centre_ra = ra;
	sky->projection.centre_dec = dec;
	projection_check_bounds(&sky->projection);
}

static void sky_zoom(Sky *sky, gfloat zoom)
{
	sky->projection.fov *= zoom;
	projection_check_bounds(&sky->projection);
	legend_update_fov(sky->legend_widget, sky->projection.fov, 
		sky->projection.clip_mag_faint);
}

static gboolean sky_key_press_event(GtkWidget *widget, GdkEventKey *event,
				gpointer user_data)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	
	switch (event->keyval) {
	case GDK_Right:
		projection_move_rel_pixels(&sky->projection, -SKY_MOVE_SIZE, 0);
	        break;
	case GDK_Left:
		projection_move_rel_pixels(&sky->projection, SKY_MOVE_SIZE, 0);
		break;
	case GDK_Down:
		projection_move_rel_pixels(&sky->projection, 0, -SKY_MOVE_SIZE);
		break;
	case GDK_Up:
		projection_move_rel_pixels(&sky->projection, 0, SKY_MOVE_SIZE);
		break;
	case GDK_minus:
		sky_zoom(sky, SKY_ZOOM_OUT);
		break;
	case GDK_equal:
		sky_zoom(sky, SKY_ZOOM_IN);
		break;
	default:
		return TRUE;
	}

	sky->robject.type = RT_FAST;
	gtk_widget_queue_draw(widget);
	sky->is_modified = 1;
	return TRUE;
}

static gboolean sky_key_release_event(GtkWidget *widget, GdkEventKey *event,
				gpointer user_data)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	
	sky->robject.type = RT_FULL;
	gtk_widget_queue_draw(widget);
  	return TRUE;
}

static gboolean sky_enter_notify_event(GtkWidget *widget,
					GdkEventCrossing *event,
					gpointer user_data)
{
	gtk_widget_grab_focus(widget);
	gdk_window_set_cursor(event->window, sky_cursor_ptr);
	return TRUE;
}

static gboolean sky_leave_notify_event(GtkWidget *widget,
					GdkEventCrossing *event,
					gpointer user_data)
{
	gdk_window_set_cursor(event->window, sky_cursor_arrow);
	return TRUE;
}

static gboolean sky_button_press_event(GtkWidget *widget, 
						GdkEventButton *event,
						gpointer user_data)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	GdkModifierType state;

	switch (event->button) {
	case 1:
		gdk_window_get_pointer (event->window, &sky->pointer_x, 
					&sky->pointer_y, &state);
		sky->robject.type = RT_FAST;
		sky->pointer_drag = 1;
		break;
	}
	
  	return TRUE;
}

static gboolean sky_button_release_event(GtkWidget *widget, 
						GdkEventButton *event,
						gpointer user_data)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	
	switch (event->button) {
	case 1:
		sky->robject.type = RT_FULL;
		sky->pointer_drag = 0;
		gtk_widget_queue_draw(widget);
		break;
	}
	
  	return TRUE;
}

static gboolean sky_scroll_event(GtkWidget *widget,
					GdkEventScroll *event,
					gpointer user_data)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);

	switch (event->direction) {
	case GDK_SCROLL_UP:
		sky_zoom(sky, SKY_ZOOM_IN);
		break;
	case GDK_SCROLL_DOWN:
		sky_zoom(sky, SKY_ZOOM_OUT);
		break;
	case GDK_SCROLL_LEFT:
	case GDK_SCROLL_RIGHT:
		break;
	}
	
	sky->robject.type = RT_FAST;
	sky->redraw_pending = 1;
	sky->is_modified = 1;
	gtk_widget_queue_draw(widget);
	
	/* add timeout to draw detail when scroll completes */
	g_timeout_add(20, sky_complete_scroll_render, widget);
	return TRUE;
}

static gboolean sky_motion_notify_event(GtkWidget *widget,
					GdkEventMotion  *event,
					gpointer user_data)
{
	GdkModifierType state;
	struct render_coord rc;
	struct ln_equ_posn pos;
	struct astrodb_object *object;
	gint x,y;
	Sky *sky = NOVA_SKY_GET_PRIVATE(widget);
	
	sky->robject.coord[0].posn = &pos;
	gdk_window_get_pointer(event->window, &x, &y, &state);
	
	/* drag the sky to pointer position */
	if (sky->pointer_drag) {
		projection_move_rel_pixels(&sky->projection, 
						x - sky->pointer_x, 
						y - sky->pointer_y);
		sky->pointer_x = x;
		sky->pointer_y = y;
		gtk_widget_queue_draw(widget);
		sky->is_modified = 1;
	}
	
	/* update the legend position */
	if (event->is_hint) {
		sky->robject.coord[0].x = rc.x = x;
		sky->robject.coord[0].y = rc.y = y;
		sky_get_posn(sky, &sky->robject.coord[0]);
		legend_update_pointer(sky->legend_widget,
			sky->robject.coord[0].posn->ra,
			sky->robject.coord[0].posn->dec);
			
			/* update or clear legend object */
			object = tile_get_object_at(sky->tile, &rc);
			legend_set_object(sky->legend_widget, object);
	}

	return TRUE;
}

void on_sky_markers_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dlg, *sky_widget = (GtkWidget *)user_data;
	Sky *sky = NOVA_SKY_GET_PRIVATE(sky_widget);
	gint result = 0;

	dlg = ui_get_dialog("SkyMarkersDialog");
	if (dlg == NULL)
		return;
	
	ui_markers_dlg_init(dlg, &sky->marker_settings);
	
	do {
		result = gtk_dialog_run(GTK_DIALOG(dlg));
		if (result == GTK_RESPONSE_OK || result == GTK_RESPONSE_APPLY)
			gtk_widget_queue_draw(sky_widget);
	} while (result == GTK_RESPONSE_APPLY || result == GTK_RESPONSE_HELP);
	
	gtk_widget_destroy(dlg);
}

static int render_stars(gpointer data, gpointer udata)
{
	Sky *sky = udata;
	struct astrodb_slist *objects = data;
	struct sky2kv4_object *star; 

	while (objects) {
		/* get object --> transform --> render_object */
		star = (struct sky2kv4_object*)objects->data;
		
		sky->robject.object = star;
		
		if (star->object.posn_mag.Vmag == FP_NAN)
			goto next;
		
		sky->robject.coord[0].posn = &star->object.posn_mag.ra;
		sky->projection.trans->sky_to_proj_equ(&sky->projection, 
			&sky->robject.coord[0]);
		if (projection_is_visible0(&sky->projection, &sky->robject)) {
			
			star_object_render(&sky->robject);
			if (sky->robject.type == RT_FULL)
				tile_add_object(sky->tile, &sky->robject);
		}
next:
		objects = astrodb_slist_next(objects);
	}
	return 0;
}

static void render_default(Sky *sky)
{	
	gfloat mag_bright, mag_faint, mag_index;
	struct astrodb_slist *objects = NULL;

	/* are we only rendering bright objects */
	if (sky->robject.type == RT_FAST)
		mag_faint = sky->projection.clip_mag_faint - 
			SKY_UPDATE_MAG_DIFF;
	else
		mag_faint = sky->projection.clip_mag_faint;
	
	mag_bright = sky->projection.clip_mag_bright;
	
	/* render_object in magnitude bands from 
	 * brightest --> faintest_magnitude */
	for (mag_index = mag_bright; mag_index < mag_faint; mag_index++) {
		
		/* clip the catalog in mag bands */
		astrodb_table_unclip(sky->default_star_table);
		astrodb_table_clip_on_fov(sky->default_star_table, 
					sky->projection.centre_az, 
					sky->projection.centre_alt, 
					sky->projection.fov, 
					mag_index, mag_index -1);
		
		if (astrodb_table_get_objects(sky->default_star_table,
						&objects, ADB_SMEM) == 0)
			continue;

		astrodb_slist_foreach(objects, render_stars, sky);
		astrodb_table_put_objects(objects);
		objects = NULL; 
	}
}

/*
 * Render the virtual sky
 *
 * There are 2 different render_object types:-
 *
 * RT_FULL -->  render_object everthing that is visible
 * RT_FAST -->  render_object bright items that are visible
 */
static void vsky_render(Sky *sky)
{
	/* render_object every item that is visible */
	if (sky->marker_settings.show_const_figures)
		constellation_render_lines(sky);
	
	if (sky->marker_settings.show_grid)
		markers_grid_render(sky);
				
	if (sky->marker_settings.show_const_bounds)
		constellation_render_bounds(sky);
	if (sky->marker_settings.show_const_names)
		constellation_render_names(sky);
		
	if (sky->marker_settings.show_horizon)
		markers_horizon_render(sky);
	if (sky->marker_settings.show_horizon_NEWS)
		markers_horizon_NEWS_render(sky);
	if (sky->marker_settings.show_ecliptic)
		markers_ecliptic_render(sky);
	if (sky->marker_settings.show_galactic_poles)
		markers_galactic_poles_render(sky);
	
	if (sky->solar_system_settings.planets.show_object)
		planets_render(sky);
	if (sky->solar_system_settings.solar.show_object)
		solar_render(sky);
	if (sky->solar_system_settings.lunar.show_object)
		lunar_render(sky);
				
	render_default(sky);   
}


static void virtual_sky_class_init (GtkSkyClass *class)
{
	GObjectClass *obj_class;
	GtkWidgetClass *widget_class;

	obj_class = G_OBJECT_CLASS (class);
	widget_class = GTK_WIDGET_CLASS (class);

	widget_class->expose_event = virtual_sky_expose;

	g_type_class_add_private (obj_class, sizeof(Sky));
}

static void virtual_sky_init (GtkSky *gtk_sky)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(gtk_sky);
	
	sky->default_star_table = db_get_default_table("star");
	projection_set_flip(&sky->projection, PF_NONE);
	sky->projection.centre_ra = 0;
	sky->projection.centre_dec = 0;
	sky->projection.fov = 180;
	sky->projection.clip_mag_faint = 3;
	sky->projection.clip_mag_bright = -2;
	sky->projection.coords = PC_ALT_AZ;
	sky->tile = tile_init();
	
	sky->marker_settings.show_const_figures = 1;
	sky->marker_settings.show_const_bounds = 1;
	sky->marker_settings.show_grid = 1;
	sky->marker_settings.show_grid_labels = 1;
	sky->marker_settings.show_horizon = 1;
	sky->marker_settings.show_horizon_NEWS = 1;
	sky->solar_system_settings.planets.show_object = 1;
	sky->solar_system_settings.planets.show_label = 1;
	sky->solar_system_settings.lunar.show_label = 1;
	sky->solar_system_settings.lunar.show_object = 1;
	sky->solar_system_settings.solar.show_label = 1;
	sky->solar_system_settings.solar.show_object = 1;

	sky->robject.type = RT_FULL; 

	// TODO: get observer location and JD 1st run
	sky->observer.JD = ln_get_julian_from_sys();
	sky->observer.posn.lng = 3.0;
	sky->observer.posn.lat = 56.0;
	projection_set_observer(&sky->projection, &sky->observer);
	
	virtual_sky_update (gtk_sky);
	
	/* update the vsky once a second - TODO vary time based on FOV */
	g_timeout_add(1000, virtual_sky_update, gtk_sky);
}

static void sky_draw(GtkWidget *sky_widget, cairo_t *cr)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(sky_widget);
	GTimeVal start, finish;
	
	g_get_current_time(&start);
	
	tile_set_array_size(sky->tile, sky_widget->allocation.width, 
				sky_widget->allocation.height);
	tile_reset_array(sky->tile);
	
	projection_fov_bounds(&sky->projection, &sky->observer);
	sky->robject.context.pixels_per_degree = 
		sky->projection.pixels_per_degree;
	sky->robject.context.JD = sky->observer.JD;
	sky->robject.context.faintest_magnitude = 
		sky->projection.clip_mag_faint;

	sky->robject.cr = cr;
	vsky_render(sky);
	
	cairo_restore(cr);
	sky->redraw_pending = sky->is_modified = 0;
	g_get_current_time(&finish);
	//printf("finish %ld fps %ld\n", finish.tv_usec - start.tv_usec, 
	//	1000000 / (finish.tv_usec - start.tv_usec));
}

static gboolean virtual_sky_expose (GtkWidget *sky_widget,
					GdkEventExpose *event)
{
	cairo_t *cr;
	Sky *sky = NOVA_SKY_GET_PRIVATE(sky_widget);

	/* the sky is modified if we have been resized */
	if (sky->projection.sky_width != sky_widget->allocation.width ||
		sky->projection.sky_height != sky_widget->allocation.height)
		sky->is_modified = 1;
	
	/* save sky dimensions */	
	sky->projection.sky_width = sky_widget->allocation.width;
	sky->projection.sky_height = sky_widget->allocation.height;

	/* get a cairo_t */
	cr = gdk_cairo_create(sky_widget->window);
	
	/* draw night sky background */
	cairo_set_source_rgb(cr, 0, 0, 0);
	cairo_paint(cr);
	
	/* render_object sky objects */
	sky_draw(sky_widget, cr);
	cairo_destroy(cr);

	return FALSE;
}

static void virtual_sky_redraw_canvas (GtkSky *gtk_sky)
{
	GtkWidget *widget;
	GdkRegion *region;
	
	widget = GTK_WIDGET(gtk_sky);

	if (!widget->window)
		return;

	region = gdk_drawable_get_clip_region(widget->window);
	/* redraw the cairo canvas completely by exposing it */
	gdk_window_invalidate_region(widget->window, region, TRUE);
	gdk_window_process_updates(widget->window, TRUE);

	gdk_region_destroy(region);
}

static gboolean virtual_sky_update (gpointer data)
{
	GtkSky *gtk_sky = NOVA_SKY(data);
	Sky *sky = NOVA_SKY_GET_PRIVATE(gtk_sky);
	
	sky->observer.JD = ln_get_julian_from_sys();
	virtual_sky_redraw_canvas(gtk_sky);

	return TRUE; /* keep running this event */
}

GtkWidget * virtual_sky_new (void)
{
	return g_object_new(NOVA_TYPE_SKY, NULL);
}


void sky_connect_signals(GtkWidget *gtk_sky)
{
	g_signal_connect(gtk_sky, "key_press_event",
			G_CALLBACK (sky_key_press_event), NULL);
	g_signal_connect(gtk_sky, "key_release_event",
			G_CALLBACK (sky_key_release_event), NULL);
	g_signal_connect(gtk_sky, "button_press_event",
			G_CALLBACK (sky_button_press_event), NULL);
	g_signal_connect(gtk_sky, "button_release_event",
			G_CALLBACK (sky_button_release_event), NULL);		
	g_signal_connect(gtk_sky, "motion_notify_event",
			G_CALLBACK (sky_motion_notify_event), NULL);
	g_signal_connect(gtk_sky, "enter_notify_event",
			G_CALLBACK (sky_enter_notify_event), NULL);
	g_signal_connect(gtk_sky, "leave_notify_event",
			G_CALLBACK (sky_leave_notify_event), NULL);
	g_signal_connect(gtk_sky, "scroll_event",
			G_CALLBACK (sky_scroll_event), NULL);
	
	g_object_set(gtk_sky, "can-default", TRUE, "can-focus", TRUE, NULL);
	gtk_widget_add_events (gtk_sky, GDK_POINTER_MOTION_MASK |
    					GDK_POINTER_MOTION_HINT_MASK | 
					GDK_BUTTON_PRESS_MASK	| 
					GDK_BUTTON_RELEASE_MASK |
					GDK_KEY_PRESS_MASK |
					GDK_KEY_RELEASE_MASK |
					GDK_ENTER_NOTIFY_MASK |
					GDK_LEAVE_NOTIFY_MASK);
	
	sky_cursor_arrow = gdk_cursor_new(GDK_LEFT_PTR);
	sky_cursor_ptr = gdk_cursor_new(GDK_CROSSHAIR);
}

void sky_set_legend(GtkWidget *sky_widget, GtkWidget *legend_widget)
{
	Sky *sky = NOVA_SKY_GET_PRIVATE(sky_widget);
	
	sky->legend_widget = legend_widget;
	legend_set_observer(sky->legend_widget, &sky->observer);
}
