/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __SKY_H__
#define __SKY_H__

#include <gtk/gtk.h>
#include <libastrodb/astrodb.h>
#include "settings.h"
#include "projection.h"
#include "gconf.h"

G_BEGIN_DECLS

#define NOVA_TYPE_SKY \
	(virtual_sky_get_type ())
#define NOVA_SKY(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), NOVA_TYPE_SKY, GtkSky))
#define NOVA_SKY_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_CAST ((obj), NOVA_SKY, GtkSkyClass))
#define NOVA_IS_SKY(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), NOVA_TYPE_SKY))
#define NOVA_IS_SKY_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE ((obj), NOVA_TYPE_SKY))
#define NOVA_SKY_GET_CLASS \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), NOVA_TYPE_SKY, GtkSkyClass))

typedef struct _GtkSky	GtkSky;
typedef struct _GtkSkyClass	GtkSkyClass;

struct _GtkSky
{
	GtkDrawingArea parent;
};

struct _GtkSkyClass
{
	GtkDrawingAreaClass parent_class;
};

GtkWidget *virtual_sky_new(void);
void sky_connect_signals(GtkWidget *gtk_sky);
void sky_set_legend(GtkWidget *sky_widget, GtkWidget *legend_widget);

void on_sky_markers_activate(GtkMenuItem *menuitem, gpointer user_data);

typedef struct _Sky {
	
	/* observer/observatory info */
	struct observer_info observer;
	struct tm time;
	
	/* sky configuration */
	struct sky_marker_flags marker_settings;
	struct solar_system_flags solar_system_settings;
	struct deep_sky_flags deep_sky_settings;
	
	/* sky projection */
	struct projection projection;
	
	/* object datasets */
	struct astrodb_table *default_star_table;
	
	/* render data */
	struct tile_array *tile;
	struct render_object robject;
	gboolean pointer_drag;
	gint pointer_x, pointer_y;
	gint redraw_pending;
	
	/* file / print */
	gchar *filename;
	gint is_modified;
	
	/* sky legend */
	GtkWidget *legend_widget;
	
	/* UI config */
	struct ui_config *ui_config;
} Sky;


G_END_DECLS

#endif
