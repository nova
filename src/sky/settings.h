/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <glib-2.0/glib.h>
#include <libnova/libnova.h>
#include "render.h"

struct sky_marker_flags {
	guint show_const_figures;	/*!< Show constellation figures */
	guint show_const_bounds;	/*!< Show constellation boundaries */
	guint show_const_names;		/*!< Show constellation names */
	guint show_const_greek;		/*!< Show constellation greek star letters */
	guint show_grid;		/*!< Show grid */
	guint show_grid_labels;		/*!< Show grid labels */
	guint show_ecliptic;		/*!< Show ecliptic */
	guint show_galactic_poles;	/*!< Show galatic N and S poles */
	guint show_horizon;
	guint show_horizon_NEWS;
};

struct deep_sky_flags {
	struct render_flags galactic;
	guint show_stellar_sao;
	guint show_stellar_hd;
	guint show_stellar_bd;
	guint show_stellar_mag;
};

struct solar_system_flags {
	struct render_flags planets;
	struct render_flags lunar;
	struct render_flags solar;
	struct render_flags asteroids;
	struct render_flags comets;
};

struct observer_info {
	gdouble JD;			/*!< Julian Day */
	gdouble epoch;			/*!< Observing Epoch */
	gchar* location;		/*!< struct observer location name */
	gfloat elevation;		/*!< struct observer elevation */
	struct ln_lnlat_posn posn;	/*!< struct observer posn */
	guint is_sys_time;		/*!< Sky view is current system time */
	gfloat zone_diff;		/*!< Time zone difference in hours */
};

#endif
