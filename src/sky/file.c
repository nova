/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <string.h>
#include <libxml/xmlwriter.h>

#include "astro_object.h"
#include "projection.h"
#include "render.h"
#include "settings.h"
#include "sky.h"
#include "file.h"

/*
 * Sky XML settings
 */

enum xml_type {
	XT_INT = 0,
	XT_FLOAT,
	XT_DOUBLE,
	XT_STRING,
	XT_NEW_ELEM,
};

struct xml_field_info {
	gchar* name;		/* xml name */
	guint offset;		/* offset in struct */
	enum xml_type type;	/* xml coord type */
};

struct xml_struct_info {
	int size;		/* array size */
	guint offset;		/* offset in struct */
	const struct xml_field_info *ifo;	/* ptr to array */
};

static const struct xml_field_info proj_xml[] = {
	{"sky", 0, XT_NEW_ELEM},
	{"ra", noffset(struct projection, centre_ra), XT_FLOAT},
	{"dec", noffset(struct projection, centre_dec), XT_FLOAT},
	{"fov", noffset(struct projection, fov), XT_FLOAT},
};

static const struct xml_field_info observer_info_xml[] = {
	{"observer", 0, XT_NEW_ELEM},
	{"location", noffset(struct observer_info, location), XT_STRING},
	{"jd", noffset(struct observer_info, JD), XT_DOUBLE},
	{"latitude", noffset(struct observer_info, posn.lat), XT_DOUBLE},
	{"longitude", noffset(struct observer_info, posn.lng), XT_DOUBLE},
	{"epoch", noffset(struct observer_info, epoch), XT_DOUBLE},
	{"elevation", noffset(struct observer_info, elevation), XT_FLOAT},
	{"live", noffset(struct observer_info, is_sys_time), XT_INT},
};

static const struct xml_field_info projection_xml[] = {
	{"projection", 0, XT_NEW_ELEM},
//	{"projSpherical", noffset(struct projection, spherical), XT_INT},
//	{"projRADEC", noffset(struct projection, ra_dec), XT_INT},
//	{"projAltAz", noffset(struct projection, alt_az), XT_INT},
//	{"projFlipTB", noffset(struct projection, flip_tb), XT_INT},
//	{"projFlipLR", noffset(struct projection, flip_lr), XT_INT},
};

static const struct xml_field_info markers_xml[] = {
	{"markers", 0, XT_NEW_ELEM},
	{"constFigures", noffset(struct sky_marker_flags, show_const_figures), XT_INT},
	{"constBounds", noffset(struct sky_marker_flags, show_const_bounds), XT_INT},
	{"constNames", noffset(struct sky_marker_flags, show_const_names), XT_INT},
	{"constGreek", noffset(struct sky_marker_flags, show_const_greek), XT_INT},
	{"grid", noffset(struct sky_marker_flags, show_grid), XT_INT},
	{"gridLabels", noffset(struct sky_marker_flags, show_grid_labels), XT_INT},
	{"ecliptic", noffset(struct sky_marker_flags, show_galactic_poles), XT_INT},
	{"galacticPoles", noffset(struct sky_marker_flags, show_galactic_poles), XT_INT},
};

static const struct xml_field_info nearsky_xml[] = {
	{"nearSky", 0, XT_NEW_ELEM},
	{"showSun", noffset(struct solar_system_flags, solar.show_object), XT_INT},
	{"showSunOutline", noffset(struct solar_system_flags, solar.show_outline), XT_INT},
	{"showSunLabel", noffset(struct solar_system_flags, solar.show_label), XT_INT},
	{"showMoon", noffset(struct solar_system_flags, lunar.show_object), XT_INT},
	{"showMoonOutline", noffset(struct solar_system_flags, lunar.show_outline), XT_INT},
	{"showMoonLabel", noffset(struct solar_system_flags, lunar.show_label), XT_INT},
	{"showMoonPhase", noffset(struct solar_system_flags, lunar.show_phase), XT_INT},
	{"showPlanets", noffset(struct solar_system_flags, planets.show_object), XT_INT},
	{"showPlanetsOutline", noffset(struct solar_system_flags, planets.show_outline), XT_INT},
	{"showPlanetsLabel", noffset(struct solar_system_flags, planets.show_label), XT_INT},
	{"showPlanetsPhase", noffset(struct solar_system_flags, planets.show_phase), XT_INT},
	{"showAsteroids", noffset(struct solar_system_flags, asteroids.show_object), XT_INT},
	{"showAsteroidsLabel", noffset(struct solar_system_flags, asteroids.show_label), XT_INT},
	{"showAsteroidsMag", noffset(struct solar_system_flags, asteroids.show_mag_limit), XT_INT},
	{"showAsteroidsMagLimit", noffset(struct solar_system_flags, asteroids.show_mag_flag), XT_FLOAT},
	{"showAsteroidsAxis", noffset(struct solar_system_flags, asteroids.show_axis_flag), XT_INT},
	{"showAsteroidsAxisMin", noffset(struct solar_system_flags, asteroids.show_axis_min), XT_FLOAT},
	{"showAsteroidsAxisMax", noffset(struct solar_system_flags, asteroids.show_axis_max), XT_FLOAT},
	{"showComets", noffset(struct solar_system_flags, comets.show_object), XT_INT},
	{"showCometsLabel", noffset(struct solar_system_flags, comets.show_label), XT_INT},
	{"showCometsMag", noffset(struct solar_system_flags, comets.show_mag_limit), XT_INT},
	{"showCometsMagLimit", noffset(struct solar_system_flags, comets.show_mag_flag), XT_FLOAT},
	{"showCometPerihelion", noffset(struct solar_system_flags, comets.show_axis_flag), XT_INT},
	{"showCometPerihelionMin", noffset(struct solar_system_flags, comets.show_axis_min), XT_FLOAT},
	{"showCometPerihelionMax", noffset(struct solar_system_flags, comets.show_axis_max), XT_FLOAT},
};

static const struct xml_field_info deepsky_xml[] = {
	{"deepSky", 0, XT_NEW_ELEM},
	{"showLabelSAO", noffset(struct deep_sky_flags, show_stellar_sao), XT_INT},
	{"showLabelHD", noffset(struct deep_sky_flags, show_stellar_hd), XT_INT},
	{"showLabelBD", noffset(struct deep_sky_flags, show_stellar_bd), XT_INT},
	{"showLabelMag", noffset(struct deep_sky_flags, show_stellar_mag), XT_INT},
	{"showGalaxy", noffset(struct deep_sky_flags, galactic.show_object), XT_INT},
	{"showGalaxyLabel", noffset(struct deep_sky_flags, galactic.show_label), XT_INT},
	{"showGalaxyMag", noffset(struct deep_sky_flags, galactic.show_mag_flag), XT_INT},
	{"showGalaxyMagLimit", noffset(struct deep_sky_flags, galactic.show_mag_limit), XT_FLOAT},
	{"showGalaxySize", noffset(struct deep_sky_flags, galactic.show_axis_flag), XT_INT},
	{"showGalaxySizeLimit", noffset(struct deep_sky_flags, galactic.show_axis_min), XT_FLOAT},
};

static const struct xml_struct_info vsky_xml[] = {
//	{nsize(proj_xml), noffset(Sky, sky_proj), proj_xml},
	{nsize(observer_info_xml), noffset(Sky, observer), observer_info_xml},
//	{nsize(projection_xml), noffset(Sky, proj_conf), projection_xml},
	{nsize(markers_xml), noffset(Sky, marker_settings), markers_xml},
	{nsize(nearsky_xml), noffset(Sky, solar_system_settings), nearsky_xml},
	{nsize(deepsky_xml), noffset(Sky, deep_sky_settings), deepsky_xml},
};

static void sky_write(Sky* sky, xmlTextWriterPtr wrt)
{
	gint i,j;
	
	for (i = 0; i < nsize(vsky_xml); i++) {
		gpointer soffset = vsky_xml[i].offset + (gpointer)sky;
		const struct xml_field_info *ifo = vsky_xml[i].ifo;

		for (j = 0; j < vsky_xml[i].size; j++) {
			gchar attr[128];

			gpointer offset = soffset + ifo[j].offset;
			switch (ifo[j].type) {
			case XT_NEW_ELEM:
				xmlTextWriterStartElement(wrt, (guchar*)ifo[j].name);
				break;
			case XT_INT:
				sprintf(attr, "%d", *(gint*)offset);
				xmlTextWriterWriteElement(wrt, (guchar*)ifo[j].name, (guchar*)attr);
				break;
			case XT_FLOAT:
				sprintf(attr, "%f", *(gfloat*)offset);
				xmlTextWriterWriteElement(wrt, (guchar*)ifo[j].name, (guchar*)attr);
				break;
			case XT_DOUBLE:
				sprintf(attr, "%f", *(gdouble*)offset);
				xmlTextWriterWriteElement(wrt, (guchar*)ifo[j].name, (guchar*)attr);
				break;
			case XT_STRING:
				sprintf(attr, "%s", (gchar*)offset);
				xmlTextWriterWriteElement(wrt, (guchar*)ifo[j].name, (guchar*)attr);
				break;
			default:
				g_warning("invalid XT sky save type\n");
			}
		}
	xmlTextWriterEndElement(wrt);
	}
}

static void sky_read(Sky* sky, xmlDocPtr doc, xmlNodePtr node)
{
	xmlChar* attribute;

	node = node->xmlChildrenNode;
	while (node != NULL) {
		gint i,j;
	
		for(i = 0; i < nsize(vsky_xml); i++) {
			gulong soffset = vsky_xml[i].offset + (gulong)sky;
			const struct xml_field_info *ifo = vsky_xml[i].ifo;

			for(j = 0; j < vsky_xml[i].size; j++) {

				if ((!xmlStrcmp(node->name, (const xmlChar *)ifo[j].name))) {
					attribute = xmlNodeListGetString(doc, node->xmlChildrenNode, 1);
				
					switch(ifo[j].type) {
					case XT_INT:
						*(gint*)(soffset + ifo[j].offset) = g_ascii_strtoull((gchar*)attribute, NULL, 10);
						break;
					case XT_FLOAT:
						*(gfloat*)(soffset + ifo[j].offset) = g_ascii_strtoull((gchar*)attribute, NULL, 10);
						break;
					case XT_DOUBLE:
						*(gdouble*)(soffset + ifo[j].offset) = g_ascii_strtoull((gchar*)attribute, NULL, 10);
						break;
					case XT_STRING:
						*(gchar**)(soffset + ifo[j].offset) = strdup((gchar*)attribute);
						break;
					case XT_NEW_ELEM:
						break;
					}
					xmlFree(attribute);
				}
			}
		}
		node = node->next;
	}
}

guint sky_save(gpointer _sky)
{
	Sky* sky = (Sky*)_sky;
	xmlDocPtr doc = NULL;
	xmlTextWriterPtr wrt = NULL;
	gint status;
	
	/* create document */
	wrt = xmlNewTextWriterFilename(sky->filename, 0);
	if (wrt == NULL) {
		g_warning("could not create xml writer");
		return 0;
	}
	status = xmlTextWriterStartDocument(wrt, "1.0", "UTF-8", NULL);
	if (status < 0) {
		g_warning("could not start document");
		xmlFreeTextWriter(wrt);
		return 0;
	}
	xmlTextWriterStartElement(wrt, (guchar*)"virtualSky");
	
	/* write data */
	sky_write(sky, wrt);

	/* close document */
	xmlTextWriterEndElement(wrt);
	xmlTextWriterEndDocument(wrt);
	xmlFreeTextWriter(wrt);
	
	sky->is_modified = 0;
	return 1;
}

guint sky_save_as(gpointer _sky, gchar* file)
{
	Sky* sky = (Sky*)_sky;
	
	sky->filename = file;
	return sky_save(sky);
}

guint sky_print(gpointer sky)
{
}

guint sky_load(gpointer _sky, xmlDocPtr doc, xmlNodePtr node)
{
	Sky* sky = (Sky*)_sky;
	
	if (xmlStrcmp(node->name, (const xmlChar *)"virtualSky"))
		return 0;
	
	node = node->xmlChildrenNode;
	sky_read(sky, doc, node);
	
	return 1;
}

guint sky_open(gpointer _sky, gchar* file)
{
	xmlDocPtr doc;
	xmlNodePtr root;
	GString* filename;
	Sky* sky = (Sky*)_sky;
	
	sky->filename = file;
	doc = xmlParseFile(file);
	
	if (doc == NULL) {
		g_warning("could not create parse context");
		return 0;
	}
	
	root = xmlDocGetRootElement(doc);
	if (root == NULL) {
		g_warning ("could not find document root");
		xmlFreeDoc(doc);
		return 0;
	}

	if (!sky_load(sky, doc, root)) {
		g_warning("could not parse file");
		xmlFreeDoc(doc);
		return 0;
	}
	
	xmlFreeDoc(doc);
	return 1;
}
