/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 * 
 * Fast sky grid render engine. Mostly working, although probably needs
 * checking by a maths trig guru to further optimise (and clip RA line at 80).
 *
 */

#define _GNU_SOURCE /* for NAN and INF */

#include <math.h>
#include <stdio.h>

#include "astro_object.h"
#include "grid.h"

#define D2R  (1.7453292519943295769e-2)  /* deg->radian */
#define R2D  (5.7295779513082320877e1)   /* radian->deg */

#define GRID_BISECT		128.0
#define GRID_NIGHT_ALPHA	0.35
#define GRID_DAY_ALPHA		0.45
#define GRID_HALF_MAX_FOV	(PROJ_MAX_FOV / 2.0)


/* RA grid in arcsecs */
static const gdouble grid_hms_ra[] = {
	15.0 * 60.0 * 60.0, 
	15.0 * 60.0 * 10.0,
	15.0 * 60.0 * 5.0,
	15.0 * 60.0, 
	15.0 * 10.0,
	15.0 * 5.0,
	15.0,
	7.5,
	1.5,
};

/* DEC drid in arcsecs */
static const gdouble grid_dms_dec[] = {
	3600.0 * 10.0,
	3600.0 * 5.0,
	3600.0,
	1200.0,
	600.0,
	300.0,
	60.0,
	10.0,
	5.0,
	1.0,
};

static inline int equ_is_tile_visible(struct projection *proj, gint ra, gint dec)
{
	struct render_object robject;
	struct ln_equ_posn pos;
	
	pos.ra = ra * GRID_RA_TILE_SIZE;
	pos.dec = PROJ_MIN_DEC + dec * GRID_DEC_TILE_SIZE;
	robject.coord[0].posn = &pos;
	proj->trans->sky_to_proj_equ(proj, &robject.coord[0]);
	return projection_is_visible0(proj, &robject);
}

static inline int hrz_is_tile_visible(struct projection *proj, gint ra, gint dec)
{
	struct render_object robject;
	struct ln_equ_posn pos;
	
	pos.ra = ra * GRID_RA_TILE_SIZE;
	pos.dec = PROJ_MIN_DEC + dec * GRID_DEC_TILE_SIZE;
	robject.coord[0].posn = &pos;
	proj->trans->sky_to_proj_hrz(proj, &robject.coord[0]);
	return projection_is_visible0(proj, &robject);
}

static inline void mark_tiles(struct projection *proj, 
	gint ra, gint dec)
{
	gint ra_start = ra - 1, ra_end = ra + 1;
	gint dec_start = dec - 1, dec_end = dec + 1;
	
	if (ra_start < 0)
		ra_start = GRID_RA_TILES -1;
	if (ra_end == GRID_RA_TILES)
		ra_end = 0;
	if (dec_start < 0)
		dec_start = 0;
	if (dec_end == GRID_DEC_TILES)
		dec_end = GRID_DEC_TILES - 1;

	proj->grid_tile[ra][dec] = 1;
	proj->grid_tile[ra_start][dec_start] = 1;
	proj->grid_tile[ra_end][dec_start] = 1;
	proj->grid_tile[ra_start][dec_end] = 1;
	proj->grid_tile[ra_end][dec_end] = 1;
	proj->grid_tile[ra][dec_start] = 1;
	proj->grid_tile[ra][dec_end] = 1;
	proj->grid_tile[ra_start][dec] = 1;
	proj->grid_tile[ra_end][dec] = 1;
}

static void equ_get_visible_tiles(struct projection *proj)
{
	gint ra, dec;
	
	/* clear all tiles first */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 0; dec < GRID_DEC_TILES; dec++) {
			proj->grid_tile[ra][dec] = 0;
		}
	}
	
	/* check each tile */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 0; dec < GRID_DEC_TILES; dec++) {
			if (equ_is_tile_visible(proj, ra, dec))
				mark_tiles(proj, ra, dec);
		}
	}
	
	/* now check centre RA, DEC if tile size > fov */
	if (proj->fov <= GRID_RA_TILE_SIZE * 1.1)
		mark_tiles(proj, (gint)(proj->centre_az / GRID_RA_TILE_SIZE),
				(gint)(proj->centre_alt / GRID_DEC_TILE_SIZE + 9));
}

static void hrz_get_visible_tiles(struct projection *proj)
{
	gint ra, dec;
	
	/* clear all tiles first */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 0; dec < GRID_DEC_TILES; dec++) {
			proj->grid_tile[ra][dec] = 0;
		}
	}
	
	/* check each tile */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 0; dec < GRID_DEC_TILES; dec++) {
			if (hrz_is_tile_visible(proj, ra, dec))
				mark_tiles(proj, ra, dec);
		}
	}
	
	/* now check centre RA, DEC if tile size > fov */
	if (proj->fov <= GRID_RA_TILE_SIZE * 1.1)
		mark_tiles(proj, (gint)(proj->centre_az / GRID_RA_TILE_SIZE),
				(gint)(proj->centre_alt / GRID_DEC_TILE_SIZE + 9));
}

/* get grid RA step size for projection in arcsecs */
static gdouble get_ra_step_delta(struct projection *proj)
{
	gint i;
	
	for (i = 0; i < nsize(grid_hms_ra); i++) {
		if (proj->fov / 1.1 > grid_hms_ra[i] / 3600.0)
			return grid_hms_ra[i] / 3600.0;
	}
	return grid_hms_ra[i] / 3600.0;
}

/* get grid DEC step size for projection in arcsecs */
static gdouble get_dec_step_delta(struct projection *proj)
{
	gint i;
	
	for (i = 0; i < nsize(grid_dms_dec); i++) {
		if (proj->fov / 1.1 > grid_dms_dec[i] / 3600.0)
			return grid_dms_dec[i] / 3600.0;
	}
	return grid_dms_dec[i] / 3600.0;
}

static void equ_render_dec_labels_at(struct projection *proj, 
	struct render_object *robject, gdouble dec)
{
	gchar text[32];
	gdouble step_delta, delta_div = 2.0;
	struct ln_equ_posn pos_start, pos_end;
	struct ln_dms dms;
	
	step_delta = get_dec_step_delta(proj);
	
	pos_start.ra = proj->centre_az;
	pos_end.ra = proj->centre_az;
	pos_start.dec = pos_end.dec = dec;
	robject->coord[0].posn = &pos_start;
	robject->coord[1].posn = &pos_end;
	
	/* find start position */
	do {
		pos_start.ra -= step_delta;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
	} while (projection_is_visible0(proj, robject) && 
		proj->centre_az - pos_start.ra < GRID_HALF_MAX_FOV);
	pos_start.ra += step_delta;
	
	/* binary chop start */
	do {
		pos_start.ra -= step_delta / delta_div;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
		if (!projection_is_visible0(proj, robject))
			pos_start.ra += step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);
	
	/* find end position */
	do {
		pos_end.ra += step_delta;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
	} while (projection_is_visible1(proj, robject) && 
		pos_end.ra - proj->centre_az < GRID_HALF_MAX_FOV);
	pos_end.ra -= step_delta;
	
	/* binary chop end */
	delta_div = 2.0;
	do {
		pos_end.ra += step_delta / delta_div;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
		if (!projection_is_visible1(proj, robject))
			pos_end.ra -= step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);
	
	ln_deg_to_dms(dec, &dms);
	if (dms.neg)
		text[0] = '-';
	else
		text[0] = '+';
	if (dms.minutes == 0 && dms.seconds == 0.0)
		sprintf(&text[1], "%2.2dº", dms.degrees);
	else if (dms.seconds < 0.1)
		sprintf(&text[1], "%2.2dº%2.2dm", dms.degrees, dms.minutes);
	else
		sprintf(&text[1], "%2.2dº%2.2dm%2.0f", dms.degrees, dms.minutes, 
			dms.seconds);
			
	cairo_move_to(robject->cr, 
			robject->coord[0].x - 30.0, 
			robject->coord[0].y - 1.0);
	cairo_show_text(robject->cr, text);
	cairo_move_to(robject->cr, 
			robject->coord[1].x + 1.0, 
			robject->coord[1].y - 1.0);
	cairo_show_text(robject->cr, text);
}

static void equ_render_ra_labels_at(struct projection *proj, 
	struct render_object *robject, gdouble ra)
{
	gchar text[32];
	gdouble step_delta, delta_div = 2.0;
	struct ln_equ_posn pos_start, pos_end;
	struct ln_hms hms;
	
	step_delta = get_ra_step_delta(proj);
	
	pos_start.dec = proj->centre_alt;
	pos_end.dec = proj->centre_alt;
	pos_start.ra = pos_end.ra = ra;
	robject->coord[0].posn = &pos_start;
	robject->coord[1].posn = &pos_end;
	
	/* find start position */
	do {
		pos_start.dec -= step_delta;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
	} while (projection_is_visible0(proj, robject) && 
		proj->centre_alt - pos_start.dec < GRID_HALF_MAX_FOV);
	pos_start.dec += step_delta;

	/* binary chop start */
	do {
		pos_start.dec -= step_delta / delta_div;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
		if (!projection_is_visible0(proj, robject))
			pos_start.dec += step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);

	/* find end position */
	do {
		pos_end.dec += step_delta;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
	} while (projection_is_visible1(proj, robject) && 
		pos_end.dec - proj->centre_alt < GRID_HALF_MAX_FOV);
	pos_end.dec -= step_delta;
	
	/* binary chop end */
	delta_div = 2.0;
	do {
		pos_end.dec += step_delta / delta_div;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_equ(proj, &robject->coord[1]);
		if (!projection_is_visible1(proj, robject))
			pos_end.dec -= step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);

	ln_deg_to_hms(ra, &hms);
	if (hms.minutes == 0 && hms.seconds == 0.0)
		sprintf(text, "%2.2dh", hms.hours);
	else if (hms.seconds < 0.1)
		sprintf(text, "%2.0dh%2.2dm", hms.hours, hms.minutes);
	else
		sprintf(text, "%2.0dh%2.2dm%2.0f", hms.hours, hms.minutes, 
			hms.seconds);

	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, text);
	cairo_move_to(robject->cr, 
			robject->coord[1].x, 
			robject->coord[1].y + 15.0); // font size
	cairo_show_text(robject->cr, text);
}

static void hrz_render_dec_labels_at(struct projection *proj, 
	struct render_object *robject, gdouble dec)
{
	gchar text[32];
	gdouble step_delta, delta_div = 2.0;
	struct ln_equ_posn pos_start, pos_end;
	struct ln_dms dms;
	
	step_delta = get_dec_step_delta(proj);
	
	pos_start.ra = proj->centre_ra;
	pos_end.ra = proj->centre_ra;
	pos_start.dec = pos_end.dec = dec;
	robject->coord[0].posn = &pos_start;
	robject->coord[1].posn = &pos_end;
	
	/* find start position */
	do {
		pos_start.ra -= step_delta;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
	} while (projection_is_visible0(proj, robject) && 
		proj->centre_ra - pos_start.ra < GRID_HALF_MAX_FOV);
	pos_start.ra += step_delta;
	
	/* binary chop start */
	do {
		pos_start.ra -= step_delta / delta_div;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
		if (!projection_is_visible0(proj, robject))
			pos_start.ra += step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);
	
	/* find end position */
	do {
		pos_end.ra += step_delta;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
	} while (projection_is_visible1(proj, robject) && 
		pos_end.ra - proj->centre_ra < GRID_HALF_MAX_FOV);
	pos_end.ra -= step_delta;
	
	/* binary chop end */
	delta_div = 2.0;
	do {
		pos_end.ra += step_delta / delta_div;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
		if (!projection_is_visible1(proj, robject))
			pos_end.ra -= step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);
	
	ln_deg_to_dms(dec, &dms);
	if (dms.neg)
		text[0] = '-';
	else
		text[0] = '+';
	if (dms.minutes == 0 && dms.seconds == 0.0)
		sprintf(&text[1], "%2.2dº", dms.degrees);
	else if (dms.seconds < 0.1)
		sprintf(&text[1], "%2.2dº%2.2dm", dms.degrees, dms.minutes);
	else
		sprintf(&text[1], "%2.2dº%2.2dm%2.0f", dms.degrees, dms.minutes, 
			dms.seconds);
			
	cairo_move_to(robject->cr, 
			robject->coord[0].x - 30.0, 
			robject->coord[0].y - 1.0);
	cairo_show_text(robject->cr, text);
	cairo_move_to(robject->cr, 
			robject->coord[1].x + 1.0, 
			robject->coord[1].y - 1.0);
	cairo_show_text(robject->cr, text);
}

static void hrz_render_ra_labels_at(struct projection *proj, 
	struct render_object *robject, gdouble ra)
{
	gchar text[32];
	gdouble step_delta, delta_div = 2.0;
	struct ln_equ_posn pos_start, pos_end;
	struct ln_hms hms;
	
	step_delta = get_ra_step_delta(proj);
	
	pos_start.dec = proj->centre_dec;
	pos_end.dec = proj->centre_dec;
	pos_start.ra = pos_end.ra = ra;
	robject->coord[0].posn = &pos_start;
	robject->coord[1].posn = &pos_end;
	
	/* find start position */
	do {
		pos_start.dec -= step_delta;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
	} while (projection_is_visible0(proj, robject) && 
		proj->centre_dec - pos_start.dec < GRID_HALF_MAX_FOV);
	pos_start.dec += step_delta;

	/* binary chop start */
	do {
		pos_start.dec -= step_delta / delta_div;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
		if (!projection_is_visible0(proj, robject))
			pos_start.dec += step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);

	/* find end position */
	do {
		pos_end.dec += step_delta;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
	} while (projection_is_visible1(proj, robject) && 
		pos_end.dec - proj->centre_dec < GRID_HALF_MAX_FOV);
	pos_end.dec -= step_delta;
	
	/* binary chop end */
	delta_div = 2.0;
	do {
		pos_end.dec += step_delta / delta_div;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[1]);
		if (!projection_is_visible1(proj, robject))
			pos_end.dec -= step_delta / delta_div;
		delta_div *= 2.0;
	} while (delta_div < GRID_BISECT);

	ln_deg_to_hms(ra, &hms);
	if (hms.minutes == 0 && hms.seconds == 0.0)
		sprintf(text, "%2.2dh", hms.hours);
	else if (hms.seconds < 0.1)
		sprintf(text, "%2.0dh%2.2dm", hms.hours, hms.minutes);
	else
		sprintf(text, "%2.0dh%2.2dm%2.0f", hms.hours, hms.minutes, 
			hms.seconds);

	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, text);
	cairo_move_to(robject->cr, 
			robject->coord[1].x, 
			robject->coord[1].y + 15.0); // font size
	cairo_show_text(robject->cr, text);
}

void equ_render_dec_labels(struct render_object *robject, 
	struct projection *proj, gdouble dec_start, gdouble dec_end)
{
	gdouble step, step_delta;
	struct ln_equ_posn pos1;
	
	pos1.ra = proj->centre_az;
	
	step_delta = get_dec_step_delta(proj);
	robject->coord[0].posn = &pos1;
	
	for (step = dec_start; step <= dec_end; step += step_delta) { 
		pos1.dec = step;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		equ_render_dec_labels_at(proj, robject, step);
	}
}

void equ_render_ra_labels(struct render_object *robject, 
	struct projection *proj, gdouble ra_start, gdouble ra_end)
{
	gdouble step, step_delta;
	struct ln_equ_posn pos1;

	pos1.dec = proj->centre_alt;
	
	step_delta = get_ra_step_delta(proj);
	robject->coord[0].posn = &pos1;
	
	for (step = ra_start; step <= ra_end; step += step_delta) { 
		pos1.ra = step;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		equ_render_ra_labels_at(proj, robject, step);
	}
}

void hrz_render_dec_labels(struct render_object *robject, 
	struct projection *proj, gdouble dec_start, gdouble dec_end)
{
	gdouble step, step_delta;
	struct ln_equ_posn pos1;
	
	pos1.ra = proj->centre_ra;
	
	step_delta = get_dec_step_delta(proj);
	robject->coord[0].posn = &pos1;
	
	for (step = dec_start; step <= dec_end; step += step_delta) { 
		pos1.dec = step;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		hrz_render_dec_labels_at(proj, robject, step);
	}
}

void hrz_render_ra_labels(struct render_object *robject, 
	struct projection *proj, gdouble ra_start, gdouble ra_end)
{
	gdouble step, step_delta;
	struct ln_equ_posn pos1;

	pos1.dec = proj->centre_dec;
	
	step_delta = get_ra_step_delta(proj);
	robject->coord[0].posn = &pos1;
	
	for (step = ra_start; step <= ra_end; step += step_delta) { 
		pos1.ra = step;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		hrz_render_ra_labels_at(proj, robject, step);
	}
}

static inline void equ_ra_line(struct render_object *robject, 
	struct projection *proj, struct ln_equ_posn *pos, 
	struct ln_equ_posn *end, gdouble divs)
{
	gint i = divs;
	gdouble step = (end->dec - pos->dec) / divs;

	/* draw to RA end */
	for (pos->dec = pos->dec + step; i > 0; pos->dec += step, i--) {

		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		cairo_line_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	}
}

static inline void equ_dec_line(struct render_object *robject, 
	struct projection *proj, struct ln_equ_posn *pos, 
	struct ln_equ_posn *end, gdouble divs)
{
	gint i = divs;
	gdouble step = (end->ra - pos->ra) / divs;

	/* draw to DEC end */
	for (pos->ra = pos->ra + step; i > 0; pos->ra += step, i--) {

		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		cairo_line_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	}
}

static inline void hrz_ra_line(struct render_object *robject, 
	struct projection *proj, struct ln_equ_posn *pos, 
	struct ln_equ_posn *end, gdouble divs)
{
	gint i = divs;
	gdouble step = (end->dec - pos->dec) / divs;

	/* draw to RA end */
	for (pos->dec = pos->dec + step; i > 0; pos->dec += step, i--) {

		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		cairo_line_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	}
}

static inline void hrz_dec_line(struct render_object *robject, 
	struct projection *proj, struct ln_equ_posn *pos, 
	struct ln_equ_posn *end, gdouble divs)
{
	gint i = divs;
	gdouble step = (end->ra - pos->ra) / divs;

	/* draw to DEC end */
	for (pos->ra = pos->ra + step; i > 0; pos->ra += step, i--) {

		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		cairo_line_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	}
}

static inline void equ_render_square(struct render_object *robject, 
	struct projection *proj, gint ra, gint dec, gdouble divs)
{
	gdouble ra_step_delta, dec_step_delta;
	struct ln_equ_posn pos, start_pos, end_pos;
	
	ra_step_delta = get_ra_step_delta(proj);
	dec_step_delta = get_dec_step_delta(proj);
	robject->coord[0].posn = &pos;
	
	/* start corner */
	start_pos.ra = pos.ra = ra * GRID_RA_TILE_SIZE;
	start_pos.dec = pos.dec = PROJ_MIN_DEC + dec * GRID_DEC_TILE_SIZE;
	
	/* end corner */
	end_pos.ra = start_pos.ra + GRID_RA_TILE_SIZE;
	end_pos.dec = start_pos.dec + GRID_DEC_TILE_SIZE;

	/* RA lines */
	for (; pos.ra <= end_pos.ra; pos.ra += ra_step_delta) {
		
		/* move to RA start */
		pos.dec = start_pos.dec;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		cairo_move_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	
		equ_ra_line(robject, proj, &pos, &end_pos, divs);
	}
	
	/* DEC lines */
	for (pos.dec = start_pos.dec; pos.dec <= end_pos.dec; 
		pos.dec += dec_step_delta) {
		
		/* move to DEC start */
		pos.ra = start_pos.ra;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		cairo_move_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	
		equ_dec_line(robject, proj, &pos, &end_pos, divs);
	}	
}

static inline void hrz_render_square(struct render_object *robject, 
	struct projection *proj, gint ra, gint dec, gdouble divs)
{
	gdouble ra_step_delta, dec_step_delta;
	struct ln_equ_posn pos, start_pos, end_pos;
	
	ra_step_delta = get_ra_step_delta(proj);
	dec_step_delta = get_dec_step_delta(proj);
	robject->coord[0].posn = &pos;
	
	/* start corner */
	start_pos.ra = pos.ra = ra * GRID_RA_TILE_SIZE;
	start_pos.dec = pos.dec = PROJ_MIN_DEC + dec * GRID_DEC_TILE_SIZE;
	
	/* end corner */
	end_pos.ra = start_pos.ra + GRID_RA_TILE_SIZE;
	end_pos.dec = start_pos.dec + GRID_DEC_TILE_SIZE;

	/* RA lines */
	for (; pos.ra <= end_pos.ra; pos.ra += ra_step_delta) {
		
		/* move to RA start */
		pos.dec = start_pos.dec;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		cairo_move_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	
		hrz_ra_line(robject, proj, &pos, &end_pos, divs);
	}
	
	/* DEC lines */
	for (pos.dec = start_pos.dec; pos.dec <= end_pos.dec; 
		pos.dec += dec_step_delta) {
		
		/* move to DEC start */
		pos.ra = start_pos.ra;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		cairo_move_to(robject->cr, 
			robject->coord[0].x, robject->coord[0].y);
	
		hrz_dec_line(robject, proj, &pos, &end_pos, divs);
	}	
}

void equ_render_grid(struct render_object *robject, struct projection *proj,
	gint labels, gdouble divs)
{
	gint ra, dec;
	
	/* render grid squares */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 1; dec < GRID_DEC_TILES - 1; dec++)
			if (proj->grid_tile[ra][dec])
				equ_render_square(robject, proj, ra, dec, divs);
	}
	
	if (!labels)
		return;

	/* render DEC labels */
	for (dec = 1; dec < GRID_DEC_TILES - 1; dec++) {
		for (ra = 0; ra < GRID_RA_TILES; ra++) {
			if (proj->grid_tile[ra][dec]) {
				gdouble dec_s = (dec - 9) * GRID_DEC_TILE_SIZE, 
					dec_e = dec_s + GRID_DEC_TILE_SIZE;
			
				if (dec_e >= 80.0)
					dec_e = 80.0;
			
				equ_render_dec_labels(robject, proj, dec_s, dec_e);
				break;
			}
		}
	}
	
	/* render RA labels */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 1; dec < GRID_DEC_TILES - 1; dec++)
			if (proj->grid_tile[ra][dec]) {
				gdouble ra_s = ra * GRID_RA_TILE_SIZE, 
					ra_e = ra_s + GRID_RA_TILE_SIZE;
				
				if (ra_e >= PROJ_MAX_RA)
					ra_e = 0.0;
				
				equ_render_ra_labels(robject, proj, ra_s, ra_e);
				break;
			}
	}
}

void hrz_render_grid(struct render_object *robject, struct projection *proj,
	gint labels, gdouble divs)
{
	gint ra, dec;
	
	/* render grid squares */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 1; dec < GRID_DEC_TILES - 1; dec++)
			if (proj->grid_tile[ra][dec])
				hrz_render_square(robject, proj, ra, dec, divs);
	}
	
	if (!labels)
		return;
	
	/* render DEC labels */
	for (dec = 1; dec < GRID_DEC_TILES - 1; dec++) {
		for (ra = 0; ra < GRID_RA_TILES; ra++) {
			if (proj->grid_tile[ra][dec]) {
				gdouble dec_s = (dec - 9) * GRID_DEC_TILE_SIZE, 
					dec_e = dec_s + GRID_DEC_TILE_SIZE;
			
				if (dec_e >= 80.0)
					dec_e = 80.0;
			
				hrz_render_dec_labels(robject, proj, dec_s, dec_e);
				break;
			}
		}
	}
	
	/* render RA labels */
	for (ra = 0; ra < GRID_RA_TILES; ra++) {
		for (dec = 1; dec < GRID_DEC_TILES - 1; dec++)
			if (proj->grid_tile[ra][dec]) {
				gdouble ra_s = ra * GRID_RA_TILE_SIZE, 
					ra_e = ra_s + GRID_RA_TILE_SIZE;
				
				if (ra_e >= PROJ_MAX_RA)
					ra_e = 0.0;
				
				hrz_render_ra_labels(robject, proj, ra_s, ra_e);
				break;
			}
	}
}

static inline void render_ecliptic(struct render_object *robject, 
					struct projection *proj)
{
	/* TODO */
#if 0	
	gdouble step;
	struct ln_equ_posn pos1;
	int start_visible, end_visible;

	pos1.ra = 0;
	pos1.dec = 0;
	robject->coord[0].posn = &pos1;
	proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
	start_visible = projection_is_visible0(proj, robject);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	
	for (step = 10; step <= PROJ_MAX_RA; step += 10) { 
		pos1.ra = step;
		proj->trans->sky_to_proj_equ(proj, &robject->coord[0]);
		end_visible = projection_is_visible0(proj, robject);
		if (start_visible || end_visible)
			cairo_line_to(robject->cr, 
				robject->coord[0].x, 
				robject->coord[0].y);
		else
			cairo_move_to(robject->cr, 
				robject->coord[0].x, 
				robject->coord[0].y);
		start_visible = end_visible;
	}
#endif
}

static inline void render_horizon(struct render_object *robject, 
					struct projection *proj)
{
	gdouble step, size;
	struct ln_equ_posn pos1;
	int start_visible, end_visible;
	double dashes[] = {3.0, 1.0, 3.0, 1.0};
	
	cairo_save(robject->cr);
	cairo_set_source_rgba(robject->cr, 0.6, 0.6, 0.6, 0.7);
	cairo_set_dash (robject->cr, dashes, 4, 0);
	
	pos1.ra = 0;
	pos1.dec = 0;
	robject->coord[0].posn = &pos1;
	
	proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
	start_visible = projection_is_visible0(proj, robject);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	
	if (robject->type == RT_FAST)  {
		cairo_set_tolerance(robject->cr, 1.0);
		size = 10.0;
	} else {
		cairo_set_tolerance(robject->cr, 0.1);
		size = 3.0;
	}
	
	for (step = 10; step <= PROJ_MAX_RA; step += size) { 
		pos1.ra = step;
		proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
		end_visible = projection_is_visible0(proj, robject);
		if (start_visible || end_visible)
			cairo_line_to(robject->cr, 
				robject->coord[0].x, 
				robject->coord[0].y);
		else
			cairo_move_to(robject->cr, 
				robject->coord[0].x, 
				robject->coord[0].y);
		start_visible = end_visible;
	}
	cairo_stroke(robject->cr);
	
	if (robject->type == RT_FAST)
		cairo_set_tolerance(robject->cr, 0.1); /* do we need this */
	cairo_restore(robject->cr);
}

static inline void render_horizon_NEWS(struct render_object *robject, 
					struct projection *proj)
{
	struct ln_equ_posn pos1;
	
	cairo_save(robject->cr);
	cairo_set_font_size (robject->cr, 20.0);
	cairo_set_source_rgba(robject->cr, 1, 0.35, 0.55, 0.7);
	robject->coord[0].posn = &pos1;
	pos1.dec = 0;
	
	/* South */
	pos1.ra = 0;
	proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, "S");
	
	/* West */
	pos1.ra = 90;
	proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, "W");
	
	/* North */
	pos1.ra = 180;
	proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, "N");
	
	/* East */
	pos1.ra = 270;
	proj->trans->sky_to_proj_hrz(proj, &robject->coord[0]);
	cairo_move_to(robject->cr, 
			robject->coord[0].x, 
			robject->coord[0].y);
	cairo_show_text(robject->cr, "E");
	
	cairo_restore(robject->cr);
}

void markers_grid_render(Sky *sky)
{
	struct render_object *robject = &sky->robject;
	struct projection *proj = &sky->projection; 
	gint labels = sky->marker_settings.show_grid_labels;
	gdouble alpha, divs;
	
	if (robject->flags.night_mode)
		alpha = GRID_NIGHT_ALPHA;
	else
		alpha = GRID_DAY_ALPHA;
	 
	cairo_save(robject->cr);
	cairo_set_source_rgba(robject->cr, 0, 0.35, 0.55, alpha);
	cairo_set_font_size (robject->cr, 13.0);
	
	if (robject->type == RT_FAST)  {
		cairo_set_tolerance(robject->cr, 1.0);
		divs = 6.0;
	} else {
		cairo_set_tolerance(robject->cr, 0.1);
		divs = 10.0;
	}
	
	switch (proj->grid_coords) {
	case PC_RA_DEC:
		equ_get_visible_tiles(proj);
		equ_render_grid(robject, proj, labels, divs);
		break;
	case PC_ALT_AZ:
		hrz_get_visible_tiles(proj);
		hrz_render_grid(robject, proj, labels, divs);
		break;
	}
	cairo_stroke(robject->cr);
	
	if (robject->type == RT_FAST)
		cairo_set_tolerance(robject->cr, 0.1); /* do we need this */
	cairo_restore(robject->cr);
}

void markers_horizon_render(Sky *sky)
{
	struct render_object *robject = &sky->robject;
	struct projection *proj = &sky->projection; 
	
	render_horizon(robject, proj);
}

void markers_horizon_NEWS_render(Sky *sky)
{
	struct render_object *robject = &sky->robject;
	struct projection *proj = &sky->projection; 
	
	render_horizon_NEWS(robject, proj);
}

void markers_ecliptic_render(Sky *sky)
{
	struct render_object *robject = &sky->robject;
	struct projection *proj = &sky->projection; 
	
	render_ecliptic(robject, proj);
}

void markers_galactic_poles_render(Sky *sky)
{
}
