/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __TILE_H
#define __TILE_H

#include <glib-2.0/glib.h>
#include "astro_object.h"

#define ARRAY_X_SIZE		30
#define ARRAY_Y_SIZE		30
#define TILE_NODES	      2048

struct tile_node {
	gint x;
	gint y;
	gdouble size;
	gpointer data;
	gpointer next;
};

struct tile_array {
	gpointer tile[ARRAY_X_SIZE][ARRAY_Y_SIZE];
	struct tile_node node[TILE_NODES];
	gint x_coeff;
	gint y_coeff;
	gint index;
};

struct tile_array* tile_init(void);

gpointer tile_get_object_at(struct tile_array *ta, struct render_coord *rc);

gpointer tile_get_object_nearest(struct tile_array *ta,
					struct render_coord *rc, gint radius);
gint tile_get_objects_within_radius (struct tile_array *ta, 
					struct render_coord *rc, gint radius, 
					struct tile_node *objects);
				
void tile_reset_array(struct tile_array *ta);

void tile_add_object(struct tile_array* ta, struct render_object *r);

void tile_set_array_size(struct tile_array* ta, gint width, gint height);

#endif
