/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __LEGEND_H__
#define __LEGEND_H__

#include <gtk/gtk.h>
#include <libastrodb/astrodb.h>
#include <libnova/libnova.h>

#include "settings.h"

G_BEGIN_DECLS

#define NOVA_TYPE_LEGEND \
	(legend_get_type ())
#define NOVA_LEGEND(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), NOVA_TYPE_LEGEND, GtkLegend))
#define NOVA_LEGEND_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_CAST ((obj), NOVA_LEGEND, GtkLegendClass))
#define NOVA_IS_LEGEND(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), NOVA_TYPE_LEGEND))
#define NOVA_IS_LEGEND_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE ((obj), NOVA_TYPE_LEGEND))
#define NOVA_LEGEND_GET_CLASS \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), NOVA_TYPE_LEGEND, GtkLegendClass))

typedef struct _GtkLegend	GtkLegend;
typedef struct _GtkLegendClass	GtkLegendClass;

struct _GtkLegend
{
	GtkDrawingArea parent;
};

struct _GtkLegendClass
{
	GtkDrawingAreaClass parent_class;
};

GtkWidget *legend_new(void);

typedef struct _Legend {
	struct ln_equ_posn pointer_pos;
	gdouble fov;
	gdouble bright_mag, faint_mag;
	struct astrodb_object *object;
	struct observer_info *observer;
} Legend;

void legend_update_pointer(GtkWidget *gtk_legend, gdouble ra, gdouble dec);
void legend_update_fov(GtkWidget *gtk_legend, gdouble fov, gdouble faint_mag);
void legend_set_object(GtkWidget *gtk_legend, struct astrodb_object *object);
void legend_set_observer(GtkWidget *gtk_legend, struct observer_info *observer);

G_END_DECLS

#endif
