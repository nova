/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <math.h>
#include "tile.h"

#define RANGE  ((ARRAY_X_SIZE + ARRAY_Y_SIZE) / 2.0)

/*
 * This is a tile array containing TILE_ELEMENTS of tiles in a
 * ARRAY_X_SIZE * ARRAY_Y_SIZE array. Elements are created on the heap
 * at array initialisation and NOT malloc'ed when they are added.
 * This gives a performance increase at the expence of a fixed
 * tile array size.
 */

/* 
 * The ptr posn and tile to compare against.
 */
struct pointer {
	gint x;
	gint y;
	struct tile_node* e;
};

static inline gdouble get_dist(struct pointer *pointer)
{
	gint dx = pointer->x - pointer->e->x;
	gint dy = pointer->y - pointer->e->y;
	dx *= dx;
	dy *= dy;	
	return sqrt(dx + dy);
}

struct tile_array* tile_init(void)
{
	struct tile_array *ta = g_malloc0(sizeof(struct tile_array));
	return ta;
}

gpointer tile_get_object_at(struct tile_array* ta, struct render_coord *robject)
{
	struct tile_node *current_node;
	struct tile_node *closest_node;
	gdouble dist, closest = RANGE;
	struct pointer pp;
	gint x, y;
	gint exists = 0;
	
	x = robject->x / ta->x_coeff;
	y = robject->y / ta->y_coeff;

	if (x >= 0 && x < ARRAY_X_SIZE && y >= 0 && y < ARRAY_Y_SIZE) {
		pp.x = robject->x;
		pp.y = robject->y;
	
		if (ta->tile[x][y] == NULL)
			return NULL;

		closest_node = current_node = ta->tile[x][y];
		while (current_node) {
			pp.e = current_node;
			dist = get_dist(&pp);
			if (dist < closest && dist < current_node->size) {
				closest = dist;
				exists = 1;
				closest_node = current_node;
			}
			current_node = current_node->next;
		}
		if (exists)
			return closest_node->data;
	}
	return NULL;
}

gpointer tile_get_nearest_object(struct tile_array* ta, 
					struct render_coord* robject, gint radius)
{
	struct tile_node* current_node;
	struct tile_node* closest_node;
	gdouble dist, closest = RANGE;
	struct pointer pp;
	gint x,y;
	gint exists = 0;
	
	x = robject->x/ta->x_coeff;
	y = robject->y/ta->y_coeff;
	
	if(x >= 0 && x < ARRAY_X_SIZE && y >= 0 && y < ARRAY_Y_SIZE) {
		pp.x = robject->x;
		pp.y = robject->y;
	
		if (ta->tile[x][y] == NULL)
			return NULL;

		closest_node = current_node = ta->tile[x][y];
		while (current_node) {
			pp.e = current_node;
			dist = get_dist(&pp);
			if (dist < closest && dist < radius) {
				closest = dist;
				closest_node = current_node;
				exists = 1;
			}
			current_node = current_node->next;
		}
		if (exists)
			return closest_node->data;
	}
	return NULL;
}

gint tile_get_objects_within_radius (struct tile_array* ta, 
					struct render_coord *rc, gint radius, 
					struct tile_node* objects)
{
}

/* 
 * Clean the array of tile elements.
 */
void tile_reset_array(struct tile_array* ta)
{
	gint x,y;
	
	if (ta->index == 0)
		return;

	for (x = 0; x < ARRAY_X_SIZE; x++) {
		for (y = 0; y < ARRAY_Y_SIZE; y++) {
			if (ta->tile[x][y]) {
				ta->tile[x][y] = NULL;
			}
		}
	}
	ta->index = 0;
}

/*
 * Change the tile size to map to a different physical sky size.
 */
void tile_set_array_size(struct tile_array* ta, gint width, gint height)
{
	ta->x_coeff = width / ARRAY_X_SIZE;
	ta->y_coeff = height / ARRAY_Y_SIZE;
	if (ta->x_coeff == 0 || ta->y_coeff == 0)
		g_critical("tile mapping is 0");
}

void tile_add_object(struct tile_array* ta, struct render_object *robject)
{
	gint xmin, xmax, ymin, ymax;
	gint tx, ty;
	
	xmin = (robject->coord[0].x - robject->coord[0].object_size) / ta->x_coeff;
	ymin = (robject->coord[0].y - robject->coord[0].object_size) / ta->y_coeff;
	xmax = (gint)ceil((robject->coord[0].x + robject->coord[0].object_size) / ta->x_coeff);
	ymax = (gint)ceil((robject->coord[0].y + robject->coord[0].object_size) / ta->y_coeff);
	
	for (tx = xmin; tx <= xmax; tx++) {
		for (ty = ymin; ty <= ymax; ty++) {
			if (ta->index < TILE_NODES) {
				if (tx >= 0 && tx < ARRAY_X_SIZE && 
					ty >= 0 && ty < ARRAY_Y_SIZE) {
					ta->node[ta->index].data = robject->object;
					ta->node[ta->index].x = robject->coord[0].x;
					ta->node[ta->index].y = robject->coord[0].y;
					ta->node[ta->index].size = robject->coord[0].object_size;
					ta->node[ta->index].next = ta->tile[tx][ty];
					ta->tile[tx][ty] = &ta->node[ta->index];
					ta->index++;
				}
			} else
				return;
		}
	}
}
