/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <strings.h>
#include <math.h>
#include "projection.h"
#include "astro_object.h"
#include "sky.h"

#define D2R  (1.7453292519943295769e-2)  /* deg->radian */
#define R2D  (5.7295779513082320877e1)   /* radian->deg */

#define PROJ_MAG_BASE		4.2
#define PROJ_MAG_FAINT 		16
#define PROJ_MAG_BRIGHT		-2

#define PROJ_MAG_CONST 		0.007
#define PROJ_MAG_FOV_DIFF	110

static inline void ra_to_altaz(struct projection *proj, 
				struct render_coord *coord, 
				double *alt_, double *az_);

struct _mag_const {
	gfloat limit;
	gfloat coeff;
};

static struct _mag_const mag_const[] = {
	{180.0, 2.7 * PROJ_MAG_CONST},
	{160.0, 2.9 * PROJ_MAG_CONST},
	{140.0, 3.0 * PROJ_MAG_CONST},
	{130.0, 3.1 * PROJ_MAG_CONST},
	{120.0, 3.2 * PROJ_MAG_CONST},
	{110.0, 3.4 * PROJ_MAG_CONST},
	{100.0, 3.7 * PROJ_MAG_CONST},
	{90.0, 3.9 * PROJ_MAG_CONST},
	{80.0, 4.1 * PROJ_MAG_CONST},
	{70.0, 4.3 * PROJ_MAG_CONST},
	{60.0, 4.5 * PROJ_MAG_CONST},
	{50.0, 4.7 * PROJ_MAG_CONST},
	{40.0, 5.0 * PROJ_MAG_CONST},
	{35.0, 5.2 * PROJ_MAG_CONST},
	{30.0, 5.4 * PROJ_MAG_CONST},
	{25.0, 5.6 * PROJ_MAG_CONST},
	{20.0, 5.8 * PROJ_MAG_CONST},
	{16.0, 6.0 * PROJ_MAG_CONST},
	{12.0, 6.5 * PROJ_MAG_CONST},
	{10.0, 7.0 * PROJ_MAG_CONST},
	{8.0, 7.5 * PROJ_MAG_CONST},
	{5.0, 7.8 * PROJ_MAG_CONST},
	{4.0, 8.0 * PROJ_MAG_CONST},
	{3.0, 8.5 * PROJ_MAG_CONST},
	{2.0, 9.0 * PROJ_MAG_CONST},
	{1.5, 10.0 * PROJ_MAG_CONST},
	{1.0, 11.0 * PROJ_MAG_CONST},
};

gint projection_set_flip(struct projection *proj, enum projection_flip flip)
{
	switch (flip) {
	case PF_NONE:
		proj->trans = &transform_flip_none;
		break;
	case PF_TB:
		proj->trans = &transform_flip_tb;
		break;
	case PF_LR:
		proj->trans = &transform_flip_lr;
		break;
	case PF_LR_TB:
		proj->trans = &transform_flip_lr_tb;
		break;
	}

	proj->flip = flip;
	proj->clip_mag_faint = PROJ_MAG_FAINT;
	proj->clip_mag_bright = PROJ_MAG_BRIGHT;
	return 0;
}

/*! \fn void void proj_calc_fov_bounds(struct projection* projection)
*
* Calculate the boundaries, magnitude level and field of view for the
* virtual sky projection.
*/
void projection_fov_bounds(struct projection *proj, 
	struct observer_info *observer)
{
	struct render_coord coord;
	struct ln_equ_posn pos;
	gfloat mag_coeff = PROJ_MAG_CONST;
	gint i = 0;
	
	proj->sky_mid_width = proj->sky_width / 2.0; 
	proj->sky_mid_height = proj->sky_height / 2.0;
	
	if (proj->sky_width > proj->sky_height) {
		proj->pixels_per_degree = proj->sky_width / proj->fov;
		proj->pixels_per_radian = proj->pixels_per_degree / D2R;
	} else {
		proj->pixels_per_degree = proj->sky_height / proj->fov;
		proj->pixels_per_radian = proj->pixels_per_degree / D2R;
	}
	while (mag_const[i].limit > proj->fov && i < nsize(mag_const)) {
		mag_coeff = mag_const[i].coeff;
		i++;
	}
	proj->clip_mag_faint = 
		PROJ_MAG_BASE + (PROJ_MAG_FOV_DIFF - proj->fov) * mag_coeff;
		
	proj->centre_ra_rad = proj->centre_ra * D2R;
	proj->centre_dec_rad = proj->centre_dec * D2R;
	proj->centre_dec_rad_sin = sin(proj->centre_dec_rad);
	proj->centre_dec_rad_cos = cos(proj->centre_dec_rad);

	/* get mean sidereal time in hours*/
	proj->sidereal = ln_get_mean_sidereal_time(observer->JD);
	
	/* change sidereal_time from hours to radians*/
	proj->sidereal *= 2.0 * M_PI / 24.0;

	/* calc constant hour angle */
	proj->hour_angle = proj->sidereal + proj->observer_lng_rad;

	/* get centre alt:az */
	coord.posn = &pos;
	coord.x = proj->sky_mid_width;
	coord.y = proj->sky_mid_width;
	proj->trans->proj_to_sky_equ(proj, &coord);
	proj->centre_az = coord.posn->ra;
	proj->centre_alt = coord.posn->dec;
}

void projection_set_observer(struct projection *proj, 
	struct observer_info *observer)
{
	proj->observer_lat_rad = observer->posn.lat * D2R;
	proj->observer_lng_rad = observer->posn.lng * D2R;
	proj->observer_lat_cos = cos(proj->observer_lat_rad);
	proj->observer_lat_sin = sin(proj->observer_lat_rad);
}

static void move_rel_pixels_equ(struct projection *projection, 
				gfloat ra_offset, gfloat dec_offset)
{
	switch (projection->flip) {
	case PF_NONE:
		projection->centre_ra += 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec += 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_TB:
		projection->centre_ra += 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec -= 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_LR:
		projection->centre_ra -= 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec += 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_LR_TB:
		projection->centre_ra -= 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec -= 
			dec_offset / projection->pixels_per_degree;
		break;
	}

	projection_check_bounds(projection);
}

static void move_rel_pixels_hrz(struct projection *projection, 
				gfloat ra_offset, gfloat dec_offset)
{
	switch (projection->flip) {
	case PF_NONE:
		projection->centre_ra -= 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec += 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_TB:
		projection->centre_ra += 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec -= 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_LR:
		projection->centre_ra -= 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec += 
			dec_offset / projection->pixels_per_degree;
		break;
	case PF_LR_TB:
		projection->centre_ra -= 
			ra_offset / projection->pixels_per_degree;
		projection->centre_dec -= 
			dec_offset / projection->pixels_per_degree;
		break;
	}
	
	projection_check_bounds(projection);
}

void projection_move_rel_pixels(struct projection *projection, 
					gfloat ra_offset, gfloat dec_offset)
{
	switch (projection->coords) {
	case PC_RA_DEC:
		move_rel_pixels_equ(projection, ra_offset, dec_offset);
		break;
	case PC_ALT_AZ:
		move_rel_pixels_hrz(projection, ra_offset, dec_offset);
		break;
	}
}

void projection_check_bounds(struct projection *proj)
{
	if (proj->centre_ra > PROJ_MAX_RA)
		proj->centre_ra -= PROJ_MAX_RA;
	if (proj->centre_ra < PROJ_MIN_RA)
		proj->centre_ra += PROJ_MAX_RA;
	if (proj->centre_dec > PROJ_MAX_DEC)
		proj->centre_dec = PROJ_MAX_DEC;
	if (proj->centre_dec < PROJ_MIN_DEC)
		proj->centre_dec = PROJ_MIN_DEC;
	if (proj->fov > PROJ_MAX_FOV)
		proj->fov = PROJ_MAX_FOV;
	if (proj->fov < PROJ_MIN_FOV)
		proj->fov = PROJ_MIN_FOV;
}

static void sky_to_cairo_hrz(struct projection *proj, struct render_coord *coord)
{
	gdouble ra = coord->posn->ra * D2R;
	gdouble dec = coord->posn->dec * D2R;
	gdouble k, ra_delta = ra - proj->centre_ra_rad;
	gdouble sin_dec = sin(dec);
	gdouble cos_dec = cos(dec);
	gdouble cos_ra_delta = cos(ra_delta);

	k = 2.0 / (1 + proj->centre_dec_rad_sin * sin_dec + 
		proj->centre_dec_rad_cos * cos_dec * cos_ra_delta);
	
	coord->x = k * (cos_dec * sin(ra_delta));
	coord->y = k * (proj->centre_dec_rad_cos * sin_dec - 
			proj->centre_dec_rad_sin * cos_dec * cos_ra_delta);
	
	coord->x = proj->sky_mid_width + coord->x * proj->pixels_per_radian;
	coord->y = proj->sky_mid_height - coord->y * proj->pixels_per_radian;
}

static inline void ra_to_altaz(struct projection *proj, 
				struct render_coord *coord, 
				double *alt_, double *az_)
{
	gdouble ra = coord->posn->ra * D2R;
	gdouble dec = coord->posn->dec * D2R;
	gdouble sin_dec = sin(dec);
	gdouble cos_dec = cos(dec);
	gdouble H, A, Ac, As, Z, Zs, alt, az;

	/* calculate hour angle of object at observers position */
	H = proj->hour_angle - ra;

	/* formula 12.6 *; missuse of A (you have been warned) */
	A = proj->observer_lat_sin * sin_dec + 
		proj->observer_lat_cos * cos_dec * cos(H);
	alt = asin(A); /* h is alt */

	/* zenith distance, Telescope Control 6.8a */
	Z = acos(A);

	/* is'n there better way to compute that? */
	Zs = sin(Z);

	/* sane check for zenith distance; don't try to divide by 0 */

	if (fabs(Zs) < 1e-5) {

		if (dec > 0)
			az = M_PI;
		else
			az = 0.0;
		if ((dec > 0 && proj->observer_lat_rad > 0)
		   || (dec < 0 && proj->observer_lat_rad < 0))
		  	alt = M_PI_2;
		else
		  	alt = -M_PI_2;
		goto out;
	}

	/* formulas TC 6.8d Taff 1991, pp. 2 and 13 - vector transformations */
	As = (cos_dec * sin(H)) / Zs;
	Ac = (proj->observer_lat_sin * cos_dec * cos (H) - 
		proj->observer_lat_cos * sin_dec) / Zs;

	/* don't bloom at atan2 */
	if (Ac == 0 && As == 0) {

	        if (dec > 0)
			az = M_PI;
		else
			az = 0.0;
		goto out;
	}
	az = atan2 (As, Ac);
out:
	*alt_ = alt;
	*az_ = az;
}

static void sky_to_cairo_equ(struct projection *proj, struct render_coord *coord)
{
	gdouble alt, az;
	gdouble k, az_delta;
	gdouble sin_alt;
	gdouble cos_alt;
	gdouble cos_az_delta;
	
	ra_to_altaz(proj, coord, &alt, &az);
	sin_alt = sin(alt);
	cos_alt = cos(alt);
	az_delta = az - proj->centre_ra_rad;
	cos_az_delta = cos(az_delta);
	
	k = 2.0 / (1 + proj->centre_dec_rad_sin * sin_alt + 
		proj->centre_dec_rad_cos * cos_alt * cos_az_delta);
	
	coord->x = k * (cos_alt * sin(az_delta));
	coord->y = k * (proj->centre_dec_rad_cos * sin_alt - 
			proj->centre_dec_rad_sin * cos_alt * cos_az_delta);
	
	coord->x = proj->sky_mid_width + coord->x * proj->pixels_per_radian;
	coord->y = proj->sky_mid_height - coord->y * proj->pixels_per_radian;
}

static void cairo_to_sky_hrz(struct projection *proj, struct render_coord *coord)
{
	gdouble p, c, cos_c, sin_c;
	gdouble centre_ra = proj->centre_ra_rad;
	gdouble centre_dec_cos = proj->centre_dec_rad_cos;
	gdouble centre_dec_sin = proj->centre_dec_rad_sin;

	coord->x = (proj->sky_mid_width - coord->x) / proj->pixels_per_radian;
	coord->y = (proj->sky_mid_height - coord->y) / proj->pixels_per_radian;
	
	p = sqrt(coord->x * coord->x + coord->y * coord->y);
	c = 2.0 * atan2(p, 2.0);
	cos_c = cos(c);
	sin_c = sin(c);
		
	coord->posn->dec = asin(cos_c * centre_dec_sin + 
			((coord->y * sin_c * centre_dec_cos) / p));
		
	coord->posn->ra = centre_ra + 
		atan2(coord->x * sin_c, p * centre_dec_cos * cos_c - 
			coord->y * centre_dec_sin * sin_c);
		
	coord->posn->dec = coord->posn->dec * R2D;
	coord->posn->ra = coord->posn->ra * R2D;
}

static void cairo_to_sky_equ(struct projection *proj, struct render_coord *coord)
{
	gdouble p, c, cos_c, sin_c, alt, az;
	gdouble centre_ra = proj->centre_ra_rad;
	gdouble centre_dec_cos = proj->centre_dec_rad_cos;
	gdouble centre_dec_sin = proj->centre_dec_rad_sin;

	coord->x = (coord->x - proj->sky_mid_width) / proj->pixels_per_radian;
	coord->y = (proj->sky_mid_height - coord->y) / proj->pixels_per_radian;
	
	p = sqrt(coord->x * coord->x + coord->y * coord->y);
	c = 2.0 * atan2(p, 2.0);
	cos_c = cos(c);
	sin_c = sin(c);
		
	alt = asin(cos_c * centre_dec_sin + 
			((coord->y * sin_c * centre_dec_cos) / p));
		
	az = centre_ra + 
		atan2(coord->x * sin_c, p * centre_dec_cos * cos_c - 
			coord->y * centre_dec_sin * sin_c);
	
	/* equ on pg89 */
	coord->posn->ra = atan2(sin(az), 
			(cos(az) * proj->observer_lat_sin + tan(alt) * 
			proj->observer_lat_cos));
	coord->posn->dec = proj->observer_lat_sin * sin(alt) - 
		proj->observer_lat_cos * cos(alt) * cos(az);
	coord->posn->dec = asin(coord->posn->dec);

	coord->posn->ra = 
		ln_range_degrees(ln_rad_to_deg(proj->hour_angle - coord->posn->ra));
		
	coord->posn->dec = coord->posn->dec * R2D;
}

static void sky_to_cairo_tb(struct projection *proj, struct render_coord *coord)
{
	gdouble ra = coord->posn->ra * D2R;
	gdouble dec = coord->posn->dec * D2R;
	gdouble k, ra_delta = ra - proj->centre_ra_rad;
	gdouble sin_dec = sin(dec);
	gdouble cos_dec = cos(dec);
	gdouble cos_ra_delta = cos(ra_delta);

	k = 2.0 / (1 + proj->centre_dec_rad_sin * sin_dec + 
		proj->centre_dec_rad_cos * cos_dec * cos_ra_delta);
	
	coord->x = k * (cos_dec * sin(ra_delta));
	coord->y = k * (proj->centre_dec_rad_cos * sin_dec - 
			proj->centre_dec_rad_sin * cos_dec * cos_ra_delta);
	
	coord->x = proj->sky_mid_width - coord->x * proj->pixels_per_radian;
	coord->y = proj->sky_mid_height + coord->y * proj->pixels_per_radian;
}

static void cairo_to_sky_tb(struct projection *proj, struct render_coord *coord)
{
	gdouble p, c, cos_c, sin_c;
	gdouble centre_ra = proj->centre_ra_rad;
	gdouble centre_dec_cos = proj->centre_dec_rad_cos;
	gdouble centre_dec_sin = proj->centre_dec_rad_sin;

	coord->x = (proj->sky_mid_width + coord->x) / proj->pixels_per_radian;
	coord->y = (proj->sky_mid_height - coord->y) / proj->pixels_per_radian;
	
	p = sqrt(coord->x * coord->x + coord->y * coord->y);
	c = 2.0 * atan2(p, 2.0);
	cos_c = cos(c);
	sin_c = sin(c);
		
	coord->posn->dec = asin(cos_c * centre_dec_sin + 
			((coord->y * sin_c * centre_dec_cos) / p));
		
	coord->posn->ra = centre_ra + 
		atan2(coord->x * sin_c, p * centre_dec_cos * cos_c - 
			coord->y * centre_dec_sin * sin_c);
		
	coord->posn->dec = coord->posn->dec * R2D;
	coord->posn->ra = coord->posn->ra * R2D;
}

static void sky_to_cairo_lr(struct projection *proj, struct render_coord *coord)
{
	gdouble ra = coord->posn->ra * D2R;
	gdouble dec = coord->posn->dec * D2R;
	gdouble k, ra_delta = ra - proj->centre_ra_rad;
	gdouble sin_dec = sin(dec);
	gdouble cos_dec = cos(dec);
	gdouble cos_ra_delta = cos(ra_delta);

	k = 2.0 / (1 + proj->centre_dec_rad_sin * sin_dec + 
		proj->centre_dec_rad_cos * cos_dec * cos_ra_delta);
	
	coord->x = k * (cos_dec * sin(ra_delta));
	coord->y = k * (proj->centre_dec_rad_cos * sin_dec - 
			proj->centre_dec_rad_sin * cos_dec * cos_ra_delta);
	
	coord->x = proj->sky_mid_width + coord->x * proj->pixels_per_radian;
	coord->y = proj->sky_mid_height - coord->y * proj->pixels_per_radian;
}

static void cairo_to_sky_lr(struct projection *proj, struct render_coord *coord)
{
	gdouble p, c, cos_c, sin_c;
	gdouble centre_ra = proj->centre_ra_rad;
	gdouble centre_dec_cos = proj->centre_dec_rad_cos;
	gdouble centre_dec_sin = proj->centre_dec_rad_sin;

	coord->x = (proj->sky_mid_width - coord->x) / proj->pixels_per_radian;
	coord->y = (proj->sky_mid_height + coord->y) / proj->pixels_per_radian;
	
	p = sqrt(coord->x * coord->x + coord->y * coord->y);
	c = 2.0 * atan2(p, 2.0);
	cos_c = cos(c);
	sin_c = sin(c);
		
	coord->posn->dec = asin(cos_c * centre_dec_sin + 
			((coord->y * sin_c * centre_dec_cos) / p));
		
	coord->posn->ra = centre_ra + 
		atan2(coord->x * sin_c, p * centre_dec_cos * cos_c - 
			coord->y * centre_dec_sin * sin_c);
		
	coord->posn->dec = coord->posn->dec * R2D;
	coord->posn->ra = coord->posn->ra * R2D;
}

static void sky_to_cairo_lr_tb(struct projection *proj, struct render_coord *coord)
{
	gdouble ra = coord->posn->ra * D2R;
	gdouble dec = coord->posn->dec * D2R;
	gdouble k, ra_delta = ra - proj->centre_ra_rad;
	gdouble sin_dec = sin(dec);
	gdouble cos_dec = cos(dec);
	gdouble cos_ra_delta = cos(ra_delta);

	k = 2.0 / (1 + proj->centre_dec_rad_sin * sin_dec + 
		proj->centre_dec_rad_cos * cos_dec * cos_ra_delta);
	
	coord->x = k * (cos_dec * sin(ra_delta));
	coord->y = k * (proj->centre_dec_rad_cos * sin_dec - 
			proj->centre_dec_rad_sin * cos_dec * cos_ra_delta);
	
	coord->x = proj->sky_mid_width + coord->x * proj->pixels_per_radian;
	coord->y = proj->sky_mid_height + coord->y * proj->pixels_per_radian;
}

static void cairo_to_sky_lr_tb(struct projection *proj, struct render_coord *coord)
{
	gdouble p, c, cos_c, sin_c;
	gdouble centre_ra = proj->centre_ra_rad;
	gdouble centre_dec_cos = proj->centre_dec_rad_cos;
	gdouble centre_dec_sin = proj->centre_dec_rad_sin;

	coord->x = (proj->sky_mid_width + coord->x) / proj->pixels_per_radian;
	coord->y = (proj->sky_mid_height + coord->y) / proj->pixels_per_radian;
	
	p = sqrt(coord->x * coord->x + coord->y * coord->y);
	c = 2.0 * atan2(p, 2.0);
	cos_c = cos(c);
	sin_c = sin(c);
		
	coord->posn->dec = asin(cos_c * centre_dec_sin + 
			((coord->y * sin_c * centre_dec_cos) / p));
		
	coord->posn->ra = centre_ra + 
		atan2(coord->x * sin_c, p * centre_dec_cos * cos_c - 
			coord->y * centre_dec_sin * sin_c);
		
	coord->posn->dec = coord->posn->dec * R2D;
	coord->posn->ra = coord->posn->ra * R2D;
}

struct transform transform_flip_none = { 	
	.sky_to_proj_equ = sky_to_cairo_equ,
	.sky_to_proj_hrz = sky_to_cairo_hrz,
	.proj_to_sky_equ = cairo_to_sky_equ,
	.proj_to_sky_hrz = cairo_to_sky_hrz,
};

struct transform transform_flip_tb = { 	
//	.sky_to_proj = sky_to_cairo_tb,
//	.proj_to_sky = cairo_to_sky_tb,
};

struct transform transform_flip_lr = { 	
//	.sky_to_proj = sky_to_cairo_lr,
//	.proj_to_sky = cairo_to_sky_lr,
};

struct transform transform_flip_lr_tb = { 	
//	.sky_to_proj = sky_to_cairo_lr_tb,
//	.proj_to_sky = cairo_to_sky_lr_tb,
};
