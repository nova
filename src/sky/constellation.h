/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef _CONSTELLATION_H
#define _CONSTELLATION_H

#include <glib-2.0/glib.h>
#include "sky.h"

enum constellation_id {
	AND = 1, ANT, APS, AQR, AQL, ARA, ARG, ARI,
	AUR, BOO, CAE, CAM, CNC, CVN, CMA, CMI, CAP,
	CAR, CAS, CEN, CEP, CET, CHA, CIR, COL, COM,
	CRA, CRB, CRV, CRT, CRU, CYG, DEL, DOR, DRA,
	EQU, ERI, FOR, GEM, GRU, HER, HOR, HYA, HYI,
	IND, LAC, LEO, LMI, LEP, LIB, LUP, LYN, LYR,
	MEN, MIC, MON, MUS, NOR, OCT, OPH, ORI, PAV,
	PEG, PER, PHE, PIC, PSC, PSA, PUP, PYX, RET,
	SGE, SGR, SCO, SCL, SCT, SER, SEX, TAU, TEL,
	TRI, TRA, TUC, UMA, UMI, VEL, VIR, VOL, VUL, SER2,
};

void constellation_render_lines(Sky *sky);
void constellation_render_names(Sky *sky);
void constellation_render_bounds(Sky *sky);
const gchar* constellation_get_name(Sky *sky);

#endif
