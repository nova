/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __PROJECTION_H
#define __PROJECTION_H

#include <libnova/ln_types.h>
#include <glib-2.0/glib.h>

#include "render.h"
#include "settings.h"

#define PROJ_MAX_RA		360.0
#define PROJ_MIN_RA		0.0

#define PROJ_MAX_DEC		90
#define PROJ_MIN_DEC		-90

#define PROJ_MIN_FOV		0.5
#define PROJ_MAX_FOV		270.0

#define GRID_DEC_TILES	18
#define GRID_RA_TILES	24
#define GRID_DEC_TILE_SIZE	((PROJ_MAX_DEC - PROJ_MIN_DEC) / GRID_DEC_TILES)
#define GRID_RA_TILE_SIZE	(PROJ_MAX_RA / GRID_RA_TILES)

enum projection_flip {
	PF_NONE,
	PF_TB,
	PF_LR,
	PF_LR_TB,
};

enum projection_coords {
	PC_RA_DEC,
	PC_ALT_AZ,
};

struct projection;

struct transform {
	/* Equatorial coords - RA, DEC */
	void (*sky_to_proj_equ)(struct projection *projection, 
		struct render_coord *c);
	void (*proj_to_sky_equ)(struct projection *projection, 
		struct render_coord *c);
	/* horizontal coords - Alt, Az */
	void (*sky_to_proj_hrz)(struct projection *projection, 
		struct render_coord *c);
	void (*proj_to_sky_hrz)(struct projection *projection, 
		struct render_coord *c);
};

struct projection {

	/* field of view */
	gdouble centre_ra;		/*!< FOV centre RA */
	gdouble centre_dec;		/*!< FOV centre DEC */
	gdouble centre_az;		/*!< FOV centre RA */
	gdouble centre_alt;		/*!< FOV centre DEC */
	gdouble centre_ra_rad;		/*!< FOV centre RA */
	gdouble centre_dec_rad;		/*!< FOV centre DEC */
	gdouble centre_dec_rad_cos;	/*!< FOV centre DEC */
	gdouble centre_dec_rad_sin;	/*!< FOV centre DEC */
	gdouble fov;			/*!< vertical FOV size in degrees */
	
	/* sky size */
	gint sky_width;			/*!< Drawing area x size */
	gint sky_height;		/*!< Drawing area y size */
	gdouble sky_scale;		/*!< Rendering scale */
	gdouble	sky_mid_width;		/*!< width / 2 */
	gdouble sky_mid_height;		/*!< height / 2 */
	
	/* virtual sky clipping box */
	gdouble clip_mag_faint;		/*!< Minimum mag */
	gdouble clip_mag_bright;	/*!< Maximum mag */
	
	enum projection_flip flip;
	enum projection_coords coords;
	enum projection_coords grid_coords;
	
	/* sky pixels per degree scale */
	gdouble pixels_per_degree;	/*!< pixels per degree */
	gdouble pixels_per_radian;	/*!< pixels per radian */
	
	/* transforms */
	gdouble sidereal;
	struct transform* trans;
	
	/* observer */
	gdouble observer_lat_rad;
	gdouble observer_lng_rad;
	gdouble observer_lat_sin;
	gdouble observer_lat_cos;
	gdouble hour_angle;
	
	/* grid */
	int grid_tile[GRID_RA_TILES][GRID_DEC_TILES];	/*!< Visible grid subtiles */
};

extern struct transform transform_flip_none;
extern struct transform transform_flip_lr;
extern struct transform transform_flip_tb;
extern struct transform transform_flip_lr_tb;

gint projection_set_flip(struct projection *proj, enum projection_flip flip);
void projection_fov_bounds(struct projection *proj, 
	struct observer_info *observer);
void projection_check_bounds (struct projection* proj);
void projection_set_observer(struct projection *proj, 
	struct observer_info *observer);
void projection_move_rel_pixels(struct projection *projection, 
					gfloat ra_offset, gfloat dec_offset);

static inline int projection_is_visible0(struct projection* projection,
	struct render_object *r)
{
	if (r->coord[0].x < 0 || r->coord[0].x > projection->sky_width)
		return 0;
	if (r->coord[0].y < 0 || r->coord[0].y > projection->sky_height)
		return 0;
	return 1;
}

static inline int projection_is_visible1(struct projection* projection,
	struct render_object *r)
{
	if (r->coord[1].x < 0 || r->coord[1].x > projection->sky_width)
		return 0;
	if (r->coord[1].y < 0 || r->coord[1].y > projection->sky_height)
		return 0;
	return 1;
}

#endif
