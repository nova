/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <math.h>
#include <time.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libnova/utility.h>
#include <libnova/transform.h>
#include <libnova/julian_day.h>
#include <libnova/ln_types.h>
#include <libastrodb/astrodb.h>
#include "legend.h"
#include "render.h"
#include "star.h"
#include "tile.h"
#include "db.h"

#define RA_BASE		5.0
#define RA_TEXT		(RA_BASE + 40.0)
#define RA_SIZE		120
#define DEC_BASE	(RA_BASE + RA_SIZE)
#define DEC_TEXT	(DEC_BASE + 40.0)
#define DEC_SIZE	130
#define AZ_BASE		(DEC_BASE + DEC_SIZE)
#define AZ_TEXT		(AZ_BASE + 40.0)
#define AZ_SIZE		120
#define ALT_BASE	(AZ_BASE + AZ_SIZE)
#define ALT_TEXT	(ALT_BASE + 40.0)
#define ALT_SIZE	130
#define FOV_BASE	(ALT_BASE + ALT_SIZE)
#define FOV_TEXT	(FOV_BASE + 40.0)
#define FOV_SIZE	110
#define MAG_BASE	(FOV_BASE + FOV_SIZE)
#define MAG_TEXT	(MAG_BASE + 80.0)
#define MAG_SIZE	100
#define OBJECT_BASE	(MAG_BASE + MAG_SIZE)
#define OBJECT_TEXT	OBJECT_BASE + 80
#define OBJECT_SIZE	100

#define NOVA_LEGEND_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((obj), NOVA_TYPE_LEGEND, Legend))

G_DEFINE_TYPE (GtkLegend, legend, GTK_TYPE_DRAWING_AREA);

static gboolean legend_expose (GtkWidget *legend_widget,
					GdkEventExpose *event);

static void legend_class_init (GtkLegendClass *class)
{
	GObjectClass *obj_class;
	GtkWidgetClass *widget_class;

	obj_class = G_OBJECT_CLASS (class);
	widget_class = GTK_WIDGET_CLASS (class);

	widget_class->expose_event = legend_expose;

	g_type_class_add_private (obj_class, sizeof(Legend));
}

static void legend_init (GtkLegend *gtk_legend)
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(gtk_legend);
	legend->fov = 180.0;
}

static void render_pointer_position(Legend *legend, cairo_t *cr)
{
	struct ln_hms hms;
	struct ln_dms dms;
	struct ln_hrz_posn posn;
	gchar text[32], sign;
	
	cairo_move_to(cr, RA_BASE, 15.0);
	cairo_show_text(cr, "RA:");
	cairo_move_to(cr, DEC_BASE, 15.0);
	cairo_show_text(cr, "DEC:");
	cairo_move_to(cr, AZ_BASE, 15.0);
	cairo_show_text(cr, "AZ:");
	cairo_move_to(cr, ALT_BASE, 15.0);
	cairo_show_text(cr, "ALT:");
	cairo_move_to(cr, FOV_BASE, 15.0);
	cairo_show_text(cr, "FOV:");
	
	/* RA */
	ln_deg_to_hms(legend->pointer_pos.ra, &hms);
	sprintf(text, "%2.2dh%2.2dm%2.0fs", hms.hours, hms.minutes, 
			hms.seconds);
	cairo_move_to(cr, RA_TEXT, 15.0);
	cairo_show_text(cr, text);
	
	/* DEC */
	ln_deg_to_dms(legend->pointer_pos.dec, &dms);
	if (dms.neg)
		sign = '-';
	else
		sign = '+';
	sprintf(text, "%c%2.2dº%2.2dm%2.0fs", sign, dms.degrees, 
			dms.minutes, dms.seconds);	
	cairo_move_to(cr, DEC_TEXT, 15.0);
	cairo_show_text(cr, text);
	
	/* AZ */
	ln_get_hrz_from_equ(&legend->pointer_pos, &legend->observer->posn, 
				legend->observer->JD, &posn);
	ln_deg_to_hms(posn.az, &hms);
	sprintf(text, "%2.2dh%2.2dm%2.0fs", hms.hours, hms.minutes, 
			hms.seconds);
	cairo_move_to(cr, AZ_TEXT, 15.0);
	cairo_show_text(cr, text);
	
	/* ALT */
	ln_deg_to_dms(posn.alt, &dms);
	if (dms.neg)
		sign = '-';
	else
		sign = '+';
	sprintf(text, "%c%2.2dº%2.2dm%2.0fs", sign, dms.degrees, 
			dms.minutes, dms.seconds);	
	cairo_move_to(cr, ALT_TEXT, 15.0);
	cairo_show_text(cr, text);
	
	/* FOV */
	sprintf(text, "%2.2fº", legend->fov);
	cairo_move_to(cr, FOV_TEXT, 15.0);
	cairo_show_text(cr, text);
}

static void render_legend_magnitude(Legend *legend, cairo_t *cr)
{
	gchar text[32];
	
	cairo_move_to(cr, MAG_BASE, 15.0);
	cairo_show_text(cr, "Max Mag:");
	
	sprintf(text, "%2.0f", legend->faint_mag);
	
	cairo_move_to(cr, MAG_TEXT, 15.0);
	cairo_show_text(cr, text);
}

static void render_legend_object(Legend *legend, cairo_t *cr)
{
	gchar text[32];
	struct astrodb_object *object = legend->object;
	
	cairo_move_to(cr, OBJECT_BASE, 15.0);
	cairo_show_text(cr, "Object:");
	
	if (object == NULL)
		return;
	
	/* TODO: do this per type */
	sprintf(text, "%d %s", object->id, "todo");//object->name);
	
	cairo_move_to(cr, OBJECT_TEXT, 15.0);
	cairo_show_text(cr, text);
}

static void legend_draw(GtkWidget *legend_widget, cairo_t *cr)
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(legend_widget);

	cairo_set_source_rgba(cr, 0, 0.35, 0.55, 1.0);
	cairo_set_font_size (cr, 13.0);
	cairo_move_to(cr, 0.0, 0.0); 
	cairo_line_to(cr, legend_widget->allocation.width, 0.0);
	cairo_stroke(cr);
	
	render_pointer_position(legend, cr);
	render_legend_magnitude(legend, cr);
	render_legend_object(legend, cr);
	cairo_restore(cr);
}

static gboolean legend_expose(GtkWidget *legend_widget,
					GdkEventExpose *event)
{
	cairo_t *cr;

	/* get a cairo_t */
	cr = gdk_cairo_create(legend_widget->window);
	
	/* draw night legend background */
	cairo_rectangle(cr,event->area.x, event->area.y,
				event->area.width, event->area.height);
	cairo_clip(cr);
	cairo_set_source_rgb(cr, 0, 0, 0);
	cairo_paint(cr);
	
	/* render_object legend objects */
	legend_draw(legend_widget, cr);
	cairo_destroy(cr);

	return FALSE;
}

GtkWidget *legend_new (void)
{
	return g_object_new(NOVA_TYPE_LEGEND, NULL);
}

void legend_update_pointer(GtkWidget *gtk_legend, gdouble ra, gdouble dec) 
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(gtk_legend);
	legend->pointer_pos.ra = ra;
	legend->pointer_pos.dec = dec;
	gtk_widget_queue_draw(gtk_legend);
}

void legend_update_fov(GtkWidget *gtk_legend, gdouble fov, gdouble faint_mag)
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(gtk_legend);
	legend->fov = fov;
	legend->faint_mag = faint_mag;
	gtk_widget_queue_draw(gtk_legend);
}

void legend_set_object(GtkWidget *gtk_legend, struct astrodb_object *object)
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(gtk_legend);
	legend->object = object;
	gtk_widget_queue_draw(gtk_legend);
}

void legend_set_observer(GtkWidget *gtk_legend, struct observer_info *observer)
{
	Legend *legend;
	
	legend = NOVA_LEGEND_GET_PRIVATE(gtk_legend);
	legend->observer = observer;
}
