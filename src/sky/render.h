/*
 * Copyright (C) 2005 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef _RENDER_H
#define _RENDER_H

#include <glib-2.0/glib.h>
#include <cairo.h>

/* renderer magnitude bands - non planets, sun or moon */
#define RENDER_MAG_BANDS	32
#define RENDER_MAX_COORDS	3

/*
 * Level of sky rendering detail.
 */
enum render_detail
{
	RT_FAST,  /* low type, and fast */
	RT_FULL,  /* high quality type, slower */
};

/*
 * Object render_object coordinate
 */
struct render_coord {
	struct ln_equ_posn* posn; 	/* object true sky position - from db */
	gdouble x,y,z; 			/* cairo position - from transform */
	gdouble object_size;		/* object size in pixels - from render_object */
};

/*
 * Object render_object flags, determines what aspects of object are 
 * rendered. Note: If type is fast then this is ignored.
 */
struct render_flags {
	guint show_object;
	guint show_outline;
	guint show_phase;
	guint show_label;
	guint show_mag_flag;
	guint show_axis_flag;
	gfloat show_mag_limit;
	gfloat show_axis_min;
	gfloat show_axis_max;
	guint night_mode;
};

struct render_context {
	gdouble	faintest_magnitude;
	gdouble	pixels_per_degree;
	gdouble	position_angle;
	gdouble	JD;
};

/*
 * Sky rendering context
 */
struct render_object {
	/* object to be rendered */
	gpointer object; 
	
	/* render_object parameters */
	enum render_detail type;
	struct render_flags flags;
	struct render_context context;
	
	/* cairo render_object handle */
	cairo_t* cr;
	
	/* object coordinates */
	struct render_coord coord[RENDER_MAX_COORDS];
};

#endif
