/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <errno.h>

/*
 * Glade dialogs
 */
 
struct ui_glade {
	gchar *glade_file;
	const gchar *name;
	GtkWidget *widget;
	GladeXML *xml;
}; 

/* needed for linking and glade signal connection */
extern const gchar date_time_name[];
extern const gchar catalog_name[];
extern const gchar deep_sky_name[];
extern const gchar markers_name[];
extern const gchar observer_name[];
extern const gchar solar_system_name[];

static struct ui_glade splash[] = {
	{"/splash.glade",  		"Splash",		NULL, NULL},
};

static struct ui_glade ui[] = {
	{"/date-time-dlg.glade", 	date_time_name, NULL, NULL},
	{"/observer-dlg.glade", 	observer_name, 	NULL, NULL},
	{"/sky-catalog-dlg.glade", 	catalog_name, 	NULL, NULL},
	{"/sky-deep-dlg.glade", 	deep_sky_name, 	NULL, NULL},
	{"/sky-near-dlg.glade", 	solar_system_name, 	NULL, NULL},
	{"/sky-markers-dlg.glade", 	markers_name, 	NULL, NULL},
};

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

gint ui_init(void)
{
	gint i;

	for (i = 0; i < ARRAY_SIZE(ui); i++) {
		GString *interface = NULL;

		if (ui[i].widget)
			continue;

		interface = g_string_new(NOVA_GLADE_DIR);
		interface = g_string_append(interface, ui[i].glade_file);

		g_print("%s: loading %s\n", __func__, interface->str);
		ui[i].xml = glade_xml_new(interface->str, ui[i].name, NULL);
		
		g_string_free(interface, TRUE);
		
		if (!ui[i].xml) {
			g_critical("%s: failed to load\n", __func__); 
			return -EINVAL;
		}

		glade_xml_signal_autoconnect(ui[i].xml);
		ui[i].widget = glade_xml_get_widget(ui[i].xml, ui[i].name);

		if (!ui[i].widget) {
			g_critical("%s: failed to get widget %s\n", __func__,
				ui[i].name); 
			return -EINVAL;
		}

	}
	return 0;
}

GtkWidget* ui_get_dialog(gchar* name)
{
	gint i;
	
	if (ui_init()) {
    		g_critical("%s: can't load glade UI files", __func__);
		return NULL;
	}
	
	for (i = 0; i < ARRAY_SIZE(ui); i++) {
		if (!strcmp(name, ui[i].name))
			return ui[i].widget;
	}
	
	g_critical("%s: can't find glade UI %s", __func__, name);
	return NULL;
}

void ui_put_dialog(gchar* name)
{
	gint i = 0;

	for (i = 0; i < ARRAY_SIZE(ui); i++) {
		if (!strcmp(name, ui[i].name)) {
			ui[i].widget = NULL;
			g_object_unref(ui[i].xml);
			return;
		}
	}
	g_critical("%s: can't find glade UI %s", __func__, name);
}
