/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "glade.h"
#include "settings.h"

const gchar solar_system_name[] = "SkyNearDialog"; 

static GtkWidget* dlg_widget;
static struct solar_system_flags* near;

static void near_sync()
{
	GladeXML* xml = glade_get_widget_tree(dlg_widget);
	GtkCheckButton* check;
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_display_checkbutton"));
	near->solar.show_object = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_outline_checkbutton"));
	near->solar.show_outline = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_label_checkbutton"));
	near->solar.show_label = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_display_checkbutton"));
	near->lunar.show_object = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_outline_checkbutton"));
	near->lunar.show_outline = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_phase_checkbutton"));
	near->lunar.show_phase = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_label_checkbutton"));
	near->lunar.show_label = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_display_checkbutton"));
	near->planets.show_object = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_outline_checkbutton"));
	near->planets.show_outline = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_phase_checkbutton"));
	near->planets.show_phase = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_label_checkbutton"));
	near->planets.show_label = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
}

void
on_near_helpbutton_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_near_cancelbutton_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_CANCEL);
}


void
on_near_applybutton_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{
	near_sync();
}


void
on_near_okbutton_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	near_sync();
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_CANCEL);
}

void
on_skynear_dialog_destroy              (GtkObject       *object,
                                        gpointer         user_data)
{
	ui_put_dialog("SkyNearDialog");
}

void gui_near_dlg_init(GtkWidget* widget, struct solar_system_flags *flags)
{
	GladeXML* xml = glade_get_widget_tree(widget);
	GtkCheckButton* check;

	dlg_widget = widget;
	near = flags;
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_display_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->solar.show_object);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_outline_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->solar.show_outline);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "sun_label_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->solar.show_label);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_display_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->lunar.show_object);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_outline_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->lunar.show_outline);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_phase_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->lunar.show_phase);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "moon_label_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->lunar.show_label);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_display_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->planets.show_object);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_outline_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->planets.show_outline);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_phase_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->planets.show_phase);	
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "planet_label_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), near->planets.show_label);			
}
