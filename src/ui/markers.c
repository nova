/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "settings.h"
#include "glade.h"

const gchar markers_name[] = "SkyMarkersDialog"; 

static GtkWidget* dlg_widget;
struct sky_marker_flags *markers; /* need a better way */

static void markers_sync(void)
{
	GladeXML* xml = glade_get_widget_tree(dlg_widget);
	GtkRadioButton* radio;
	GtkCheckButton* check;

	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "grid_display_checkbutton"));
	markers->show_grid = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "grid_labels_checkbutton"));
	markers->show_grid_labels = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_figure_checkbutton"));
	markers->show_const_figures = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_bound_checkbutton"));
	markers->show_const_bounds = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_name_checkbutton"));
	markers->show_const_names = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_greek_checkbutton"));
	markers->show_const_greek = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "horizon_checkbutton"));
	markers->show_horizon = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
}

void
on_skymarkers_dialog_destroy           (GtkObject       *object,
                                        gpointer         user_data)
{
	ui_put_dialog(markers_name);
}

void
on_mark_helpbutton_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_mark_cancelbutton_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_CANCEL);
}


void
on_mark_applybutton_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{
	markers_sync();
}


void
on_mark_okbutton_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	markers_sync();
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_OK);
}


void ui_markers_dlg_init(GtkWidget* widget, struct sky_marker_flags *markers_)
{
	GladeXML* xml = glade_get_widget_tree(widget);
	GtkCheckButton* check;

	dlg_widget = widget;
	markers = markers_;

	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "grid_display_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_grid);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "grid_labels_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_grid_labels);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_figure_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_const_figures);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_bound_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_const_bounds);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_name_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_const_names);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "const_greek_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_const_greek);
	
	check = GTK_CHECK_BUTTON(glade_xml_get_widget(xml, "horizon_checkbutton"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), markers->show_horizon);
}
