/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libnova/libnova.h>
#include "settings.h"
#include "glade.h"

const gchar observer_name[] = "ObserverDialog"; 

static GtkWidget* dlg_widget;
static struct observer_info* observer; /* need a better way */

static void obs_sync(void)
{
	GladeXML* xml = glade_get_widget_tree(dlg_widget);
	GtkEntry* location;
	GtkSpinButton* spin;
	struct lnh_lnlat_posn hposn;
	
	location = GTK_ENTRY(glade_xml_get_widget(xml, "location_entry"));
	observer->location = strdup(gtk_entry_get_text(location));
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_deg_spinbutton"));
	hposn.lat.degrees = gtk_spin_button_get_value(spin);
	if(gtk_spin_button_get_value(spin) < 0)
		hposn.lat.neg = 1;
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_min_spinbutton"));
	hposn.lat.minutes = gtk_spin_button_get_value(spin);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_sec_spinbutton"));
	hposn.lat.seconds = gtk_spin_button_get_value(spin);

	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_deg_spinbutton"));
	hposn.lng.degrees = gtk_spin_button_get_value(spin);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_min_spinbutton"));
	hposn.lng.minutes = gtk_spin_button_get_value(spin);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_sec_spinbutton"));
	hposn.lng.seconds = gtk_spin_button_get_value(spin);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "elevation_spinbutton"));
	observer->elevation = gtk_spin_button_get_value(spin);
	
	ln_hlnlat_to_lnlat(&hposn, &observer->posn); 	
}


void
on_observer_dialog_destroy             (GtkObject       *object,
                                        gpointer         user_data)
{
	ui_put_dialog("struct observerDialog");
}

void
on_obs_helpbutton_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{
}


void
on_obs_cancelbutton_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_CANCEL);
}


void
on_obs_applybutton_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
	obs_sync();
}


void
on_obs_okbutton_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
	obs_sync();
	gtk_dialog_response(GTK_DIALOG(dlg_widget), GTK_RESPONSE_OK);	
}

void gui_obs_dlg_init(GtkWidget* widget, struct observer_info* obs)
{
	GladeXML* xml = glade_get_widget_tree(widget);
	GtkEntry* location;
	GtkSpinButton* spin;
	struct lnh_lnlat_posn hposn;
	gdouble angle;

	dlg_widget = widget;
	observer = obs;
	ln_lnlat_to_hlnlat(&observer->posn, &hposn); 
	
	location = GTK_ENTRY(glade_xml_get_widget(xml, "location_entry"));
	gtk_entry_set_text(location, observer->location);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_deg_spinbutton"));
	angle = hposn.lat.degrees;
	if (hposn.lat.neg)
		angle *= -1.0;
	gtk_spin_button_set_value(spin, angle);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_min_spinbutton"));
	gtk_spin_button_set_value(spin, hposn.lat.minutes);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "lat_sec_spinbutton"));
	gtk_spin_button_set_value(spin, hposn.lat.seconds);

	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_deg_spinbutton"));
	gtk_spin_button_set_value(spin, hposn.lng.degrees);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_min_spinbutton"));
	gtk_spin_button_set_value(spin, hposn.lng.minutes);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "long_sec_spinbutton"));
	gtk_spin_button_set_value(spin, hposn.lng.seconds);
	
	spin = GTK_SPIN_BUTTON(glade_xml_get_widget(xml, "elevation_spinbutton"));
	gtk_spin_button_set_value(spin, observer->elevation);
}
