/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <errno.h>

#include "astro_object.h"
#include "gconf.h"

struct ui_config_schema {
	const gchar *path;
	GConfValueType type;
	guint offset;
};
	
static const struct ui_config_schema schema[] = {
    {"/apps/nova/UI/width", GCONF_VALUE_INT, 
    	noffset(struct ui_config, ui_width)},
    {"/apps/nova/UI/height", GCONF_VALUE_INT, 
    	noffset(struct ui_config, ui_height)},
};

static GConfClient *gconf_client;

#define to_int(x) (*(gint*)((void*)config + x))
#define to_str(x) (*(gchar**)((void*)config + x))

gint ui_gconf_set_context(struct ui_config *config)
{
	GError* err;
	GConfValue* val = gconf_value_new(GCONF_VALUE_INT);
    	GConfValue* str = gconf_value_new(GCONF_VALUE_STRING);
    	gint i;
    
    	printf("set gconf context\n");
	for (i = 0; i < nsize(schema); i++) {
       		switch (schema[i].type) {
       		case GCONF_VALUE_INT:
       			gconf_value_set_int(val, to_int(schema[i].offset));
       			gconf_client_set(gconf_client, schema[i].path, val, &err);
       			break;
       		case GCONF_VALUE_STRING:
       			gconf_value_set_string(str, to_str(schema[i].offset));
       			gconf_client_set(gconf_client, schema[i].path, str, &err);
       			break;
       		case GCONF_VALUE_INVALID:
       		case GCONF_VALUE_FLOAT:
       		case GCONF_VALUE_BOOL:
       		case GCONF_VALUE_SCHEMA:
       		case GCONF_VALUE_LIST:
       		case GCONF_VALUE_PAIR:
       			break;
		}
	}
	gconf_client_suggest_sync(gconf_client, NULL);
	g_object_unref(gconf_client);
	return 0;
}

gint ui_gconf_get_context(struct ui_config *config)
{
	GError* err = NULL;
	gint i;
	
	g_type_init();
	gconf_client = gconf_client_get_default();

	if (!gconf_client_dir_exists(gconf_client, "/apps/nova", &err)) {
		g_warning("%s: gconf directory doesn't exist\n", __func__);
        	return -EINVAL;
	}
	
	gconf_client_preload(gconf_client, "/apps/nova", 
		GCONF_CLIENT_PRELOAD_RECURSIVE, &err);
    	
    	for (i = 0; i < nsize(schema); i++) {
       		switch (schema[i].type) {
       		case GCONF_VALUE_INT:
       			to_int(schema[i].offset) = 
       				gconf_client_get_int(gconf_client, 
       					schema[i].path, &err);
       			break;
       		case GCONF_VALUE_STRING:
       			to_str(schema[i].offset) = 
       				gconf_client_get_string(gconf_client, 
       					schema[i].path, &err);
       			break;
	       	case GCONF_VALUE_INVALID:
    	   	case GCONF_VALUE_FLOAT:
       		case GCONF_VALUE_BOOL:
       		case GCONF_VALUE_SCHEMA:
       		case GCONF_VALUE_LIST:
       		case GCONF_VALUE_PAIR:
       			break;
       		}
    	}
    	return 0;
}
