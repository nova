/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
#include <gtk/gtk.h> 

const gchar date_time_name[] = "DateTimeDialog"; 

void
on_sys_date_radiobutton_toggled        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_another_date_radiobutton_toggled    (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_day_spinbutton_changed              (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_month_spinbutton_changed            (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_year_spinbutton_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_hour_spinbutton_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_minute_spinbutton_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_second_spinbutton_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_julian_radiobutton_toggled          (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_julian_spinbutton_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_sys_zone_radiobutton_toggled        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_another_zone_radiobutton_toggled    (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_zone_combo_entry_changed            (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_epoch_spinbutton_changed            (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_date_helpbutton_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_date_cancelbutton_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_date_applybutton_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_date_okbutton_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_datetime_dialog_destroy             (GtkObject       *object,
                                        gpointer         user_data)
{

}

void
on_datetime_dialog_show                (GtkWidget       *widget,
                                        gpointer         user_data)
{

}

