/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#define _GNU_SOURCE		/* for NAN */

#include <errno.h>
#include <string.h>
#include <math.h>
#include <cairo.h>
#include "star.h"

struct star_colour_data {
	gchar *sp;
	gdouble r,g,b;
};

struct star_pixmap_cache {
	cairo_t *cr[SP_NUM][RENDER_MAG_BANDS];
	cairo_surface_t *surface[SP_NUM][RENDER_MAG_BANDS];
	gint offset[SP_NUM][RENDER_MAG_BANDS];
	gdouble object_size[SP_NUM][RENDER_MAG_BANDS];
};


struct star_render_size {
	gdouble size;
	gdouble alpha;
};

static const struct star_render_size star_sizes[RENDER_MAG_BANDS] = {
	{1.0,	0.5},
	{1.0,	0.8},
	{1.0,	1.0},
	{1.5,	1.0},
	{2.0,	1.0},
	{3.0,	1.0},
	{4.0,	1.0},
	{5.0,	1.0},
	{7.0,	1.0},
	{8.0,	1.0},
	{9.0,	1.0},
	{10.0,	1.0},
	{11.0,	1.0},
	{12.0,	1.0},
	{13.0,	1.0},
	{14.0,	1.0},
	{15.0,	1.0},
	{16.0,	1.0},
	{17.0,	1.0},
	{18.0,	1.0},
	{19.0,	1.0},
	{20.0,	1.0},
	{21.0,	1.0},
	{22.0,	1.0},
	{23.0,	1.0},
	{24.0,	1.0},
	{25.0,	1.0},
	{26.0,	1.0},
	{27.0,	1.0},
	{28.0,	1.0},
	{29.0,	1.0},
	{30.0,	1.0},
};

static struct star_pixmap_cache cache;

/* rgb star colours */
static const struct star_colour_data sp [SP_NUM] = {
	{"O5V", 0.607843, 0.690196, 1.000000},
	{"O6V", 0.635294, 0.721569, 1.000000},
	{"O7V", 0.615686, 0.694118, 1.000000},
	{"O8V", 0.615686, 0.694118, 1.000000},
	{"O9V", 0.603922, 0.698039, 1.000000},
	{"O9.5V", 0.643137, 0.729412, 1.000000},
	{"B0V", 0.611765, 0.698039, 1.000000},
	{"B0.5V", 0.654902, 0.737255, 1.000000},
	{"B1V", 0.627451, 0.713725, 1.000000},
	{"B2V", 0.627451, 0.705882, 1.000000},
	{"B3V", 0.647059, 0.725490, 1.000000},
	{"B4V", 0.643137, 0.721569, 1.000000},
	{"B5V", 0.666667, 0.749020, 1.000000},
	{"B6V", 0.674510, 0.741176, 1.000000},
	{"B7V", 0.678431, 0.749020, 1.000000},
	{"B8V", 0.694118, 0.764706, 1.000000},
	{"B9V", 0.709804, 0.776471, 1.000000},
	{"A0V", 0.725490, 0.788235, 1.000000},
	{"A1V", 0.709804, 0.780392, 1.000000},
	{"A2V", 0.733333, 0.796078, 1.000000},
	{"A5V", 0.792157, 0.843137, 1.000000},
	{"A6V", 0.780392, 0.831373, 1.000000},
	{"A7V", 0.784314, 0.835294, 1.000000},
	{"A8V", 0.835294, 0.870588, 1.000000},
	{"A9V", 0.858824, 0.878431, 1.000000},
	{"F0V", 0.878431, 0.898039, 1.000000},
	{"F2V", 0.925490, 0.937255, 1.000000},
	{"F4V", 0.878431, 0.886275, 1.000000},
	{"F5V", 0.972549, 0.968627, 1.000000},
	{"F6V", 0.956863, 0.945098, 1.000000},
	{"F7V", 0.964706, 0.952941, 1.000000},
	{"F8V", 1.000000, 0.968627, 0.988235},
	{"F9V", 1.000000, 0.968627, 0.988235},
	{"G0V", 1.000000, 0.972549, 0.988235},
	{"G1V", 1.000000, 0.968627, 0.972549},
	{"G2V", 1.000000, 0.960784, 0.949020},
	{"G4V", 1.000000, 0.945098, 0.898039},
	{"G5V", 1.000000, 0.956863, 0.917647},
	{"G6V", 1.000000, 0.956863, 0.921569},
	{"G7V", 1.000000, 0.956863, 0.921569},
	{"G8V", 1.000000, 0.929412, 0.870588},
	{"G9V", 1.000000, 0.937255, 0.866667},
	{"K0V", 1.000000, 0.933333, 0.866667},
	{"K1V", 1.000000, 0.878431, 0.737255},
	{"K2V", 1.000000, 0.890196, 0.768627},
	{"K3V", 1.000000, 0.870588, 0.764706},
	{"K4V", 1.000000, 0.847059, 0.709804},
	{"K5V", 1.000000, 0.823529, 0.631373},
	{"K7V", 1.000000, 0.780392, 0.556863},
	{"K8V", 1.000000, 0.819608, 0.682353},
	{"M0V", 1.000000, 0.764706, 0.545098},
	{"M1V", 1.000000, 0.800000, 0.556863},
	{"M2V", 1.000000, 0.768627, 0.513725},
	{"M3V", 1.000000, 0.807843, 0.505882},
	{"M4V", 1.000000, 0.788235, 0.498039},
	{"M5V", 1.000000, 0.800000, 0.435294},
	{"M6V", 1.000000, 0.764706, 0.439216},
	{"M8V", 1.000000, 0.776471, 0.427451},
	{"B1IV", 0.615686, 0.705882, 1.000000},
	{"B2IV", 0.623529, 0.701961, 1.000000},
	{"B3IV", 0.650980, 0.737255, 1.000000},
	{"B6IV", 0.686275, 0.760784, 1.000000},
	{"B7IV", 0.666667, 0.741176, 1.000000},
	{"B9IV", 0.705882, 0.772549, 1.000000},
	{"A0IV", 0.701961, 0.772549, 1.000000},
	{"A3IV", 0.745098, 0.803922, 1.000000},
	{"A4IV", 0.764706, 0.823529, 1.000000},
	{"A5IV", 0.831373, 0.862745, 1.000000},
	{"A7IV", 0.752941, 0.811765, 1.000000},
	{"A9IV", 0.878431, 0.890196, 1.000000},
	{"F0IV", 0.854902, 0.878431, 1.000000},
	{"F2IV", 0.890196, 0.901961, 1.000000},
	{"F3IV", 0.890196, 0.901961, 1.000000},
	{"F5IV", 0.945098, 0.937255, 1.000000},
	{"F7IV", 0.941176, 0.937255, 1.000000},
	{"F8IV", 1.000000, 0.988235, 0.992157},
	{"G0IV", 1.000000, 0.972549, 0.960784},
	{"G2IV", 1.000000, 0.956863, 0.949020},
	{"G3IV", 1.000000, 0.933333, 0.886275},
	{"G4IV", 1.000000, 0.960784, 0.933333},
	{"G5IV", 1.000000, 0.921569, 0.835294},
	{"G6IV", 1.000000, 0.949020, 0.917647},
	{"G7IV", 1.000000, 0.905882, 0.803922},
	{"G8IV", 1.000000, 0.913725, 0.827451},
	{"K0IV", 1.000000, 0.882353, 0.741176},
	{"K1IV", 1.000000, 0.847059, 0.670588},
	{"K2IV", 1.000000, 0.898039, 0.792157},
	{"K3IV", 1.000000, 0.858824, 0.654902},
	{"O7III", 0.619608, 0.694118, 1.000000},
	{"O8III", 0.615686, 0.698039, 1.000000},
	{"O9III", 0.619608, 0.694118, 1.000000},
	{"B0III", 0.619608, 0.694118, 1.000000},
	{"B1III", 0.619608, 0.694118, 1.000000},
	{"B2III", 0.623529, 0.705882, 1.000000},
	{"B3III", 0.639216, 0.733333, 1.000000},
	{"B5III", 0.658824, 0.741176, 1.000000},
	{"B7III", 0.670588, 0.749020, 1.000000},
	{"B9III", 0.698039, 0.764706, 1.000000},
	{"A0III", 0.737255, 0.803922, 1.000000},
	{"A3III", 0.741176, 0.796078, 1.000000},
	{"A5III", 0.792157, 0.843137, 1.000000},
	{"A6III", 0.819608, 0.858824, 1.000000},
	{"A7III", 0.823529, 0.858824, 1.000000},
	{"A8III", 0.819608, 0.858824, 1.000000},
	{"A9III", 0.819608, 0.858824, 1.000000},
	{"F0III", 0.835294, 0.870588, 1.000000},
	{"F2III", 0.945098, 0.945098, 1.000000},
	{"F4III", 0.945098, 0.941176, 1.000000},
	{"F5III", 0.949020, 0.941176, 1.000000},
	{"F6III", 0.945098, 0.941176, 1.000000},
	{"F7III", 0.945098, 0.941176, 1.000000},
	{"G0III", 1.000000, 0.949020, 0.913725},
	{"G1III", 1.000000, 0.952941, 0.913725},
	{"G2III", 1.000000, 0.952941, 0.913725},
	{"G3III", 1.000000, 0.952941, 0.913725},
	{"G4III", 1.000000, 0.952941, 0.913725},
	{"G5III", 1.000000, 0.925490, 0.827451},
	{"G6III", 1.000000, 0.925490, 0.843137},
	{"G8III", 1.000000, 0.905882, 0.780392},
	{"G9III", 1.000000, 0.905882, 0.768627},
	{"K0III", 1.000000, 0.890196, 0.745098},
	{"K1III", 1.000000, 0.874510, 0.709804},
	{"K2III", 1.000000, 0.866667, 0.686275},
	{"K3III", 1.000000, 0.847059, 0.654902},
	{"K4III", 1.000000, 0.827451, 0.572549},
	{"K5III", 1.000000, 0.800000, 0.541176},
	{"K7III", 1.000000, 0.815686, 0.556863},
	{"M0III", 1.000000, 0.796078, 0.517647},
	{"M1III", 1.000000, 0.784314, 0.474510},
	{"M2III", 1.000000, 0.776471, 0.462745},
	{"M3III", 1.000000, 0.784314, 0.466667},
	{"M4III", 1.000000, 0.807843, 0.498039},
	{"M5III", 1.000000, 0.772549, 0.486275},
	{"M6III", 1.000000, 0.698039, 0.474510},
	{"M7III", 1.000000, 0.647059, 0.380392},
	{"M8III", 1.000000, 0.654902, 0.380392},
	{"M9III", 1.000000, 0.913725, 0.603922},
	{"B2II", 0.647059, 0.752941, 1.000000},
	{"B5II", 0.686275, 0.764706, 1.000000},
	{"F0II", 0.796078, 0.850980, 1.000000},
	{"F2II", 0.898039, 0.913725, 1.000000},
	{"G5II", 1.000000, 0.921569, 0.796078},
	{"M3II", 1.000000, 0.788235, 0.466667},
	{"O9I", 0.643137, 0.725490, 1.000000},
	{"B0I", 0.631373, 0.741176, 1.000000},
	{"B1I", 0.658824, 0.756863, 1.000000},
	{"B2I", 0.694118, 0.768627, 1.000000},
	{"B3I", 0.686275, 0.760784, 1.000000},
	{"B4I", 0.733333, 0.796078, 1.000000},
	{"B5I", 0.701961, 0.792157, 1.000000},
	{"B6I", 0.749020, 0.811765, 1.000000},
	{"B7I", 0.764706, 0.819608, 1.000000},
	{"B8I", 0.713725, 0.807843, 1.000000},
	{"B9I", 0.800000, 0.847059, 1.000000},
	{"A0I", 0.733333, 0.807843, 1.000000},
	{"A1I", 0.839216, 0.874510, 1.000000},
	{"A2I", 0.780392, 0.839216, 1.000000},
	{"A5I", 0.874510, 0.898039, 1.000000},
	{"F0I", 0.792157, 0.843137, 1.000000},
	{"F2I", 0.956863, 0.952941, 1.000000},
	{"F5I", 0.858824, 0.882353, 1.000000},
	{"F8I", 1.000000, 0.988235, 0.968627},
	{"G0I", 1.000000, 0.937255, 0.858824},
	{"G2I", 1.000000, 0.925490, 0.803922},
	{"G3I", 1.000000, 0.905882, 0.796078},
	{"G5I", 1.000000, 0.901961, 0.717647},
	{"G8I", 1.000000, 0.862745, 0.654902},
	{"K0I", 1.000000, 0.866667, 0.709804},
	{"K1I", 1.000000, 0.862745, 0.694118},
	{"K2I", 1.000000, 0.827451, 0.529412},
	{"K3I", 1.000000, 0.800000, 0.501961},
	{"K4I", 1.000000, 0.788235, 0.462745},
	{"K5I", 1.000000, 0.819608, 0.603922},
	{"M0I", 1.000000, 0.800000, 0.560784},
	{"M1I", 1.000000, 0.792157, 0.541176},
	{"M2I", 1.000000, 0.756863, 0.407843},
	{"M3I", 1.000000, 0.752941, 0.462745},
	{"M4I", 1.000000, 0.725490, 0.407843},
	{"N", 1.000000, 0.615686, 0.000000},
	{"", 1.0, 1.0, 1.0}
};

static struct astrodb_schema_object star_fields[] = {
	sky2k4v_schema
};

void star_object_render(struct render_object *robject)
{
	struct sky2kv4_object *object = (struct sky2kv4_object*)robject->object;
	guint i = object->type;
	gint size;

	size = 2 + ceil(
		(robject->context.faintest_magnitude - object->object.posn_mag.Vmag) 
		* 2.5);

	if (size >= RENDER_MAG_BANDS)
		size = RENDER_MAG_BANDS - 1;
	if (size < 0)
		size = 0;
	if (i >= SP_NUM)
		i = SP_NUM - 1;

	robject->coord[0].object_size = cache.object_size[i][size];

	cairo_set_source_surface(robject->cr, cache.surface[i][size], 
				robject->coord[0].x - cache.offset[i][size], 
				robject->coord[0].y - cache.offset[i][size]);
	cairo_paint(robject->cr);
}

static gint render_pixmap(gint spi, gint mag)
{
	gint size = star_sizes[mag].size;
	gint surface_size = size + 3;
	gdouble radius = (gdouble)size / 2.0;
	gdouble centre = ((gdouble)size / 2.0) + 1.5;
	
	cache.surface[spi][mag] = 
		cairo_image_surface_create(CAIRO_FORMAT_ARGB32, surface_size,
						surface_size);
			
	if (cache.surface[spi][mag] == NULL)
		return -ENOMEM;
			
	cache.cr[spi][mag] = cairo_create(cache.surface[spi][mag]);
	if (cache.cr[spi][mag] == NULL)
		return -ENOMEM;
	
	cache.offset[spi][mag] = centre;
	cache.object_size[spi][mag] = radius * 2.0;

	/* render_object transparent background */
	cairo_set_source_rgba(cache.cr[spi][mag], 0.0, 0.0, 0.0, 0.0);  
	cairo_rectangle(cache.cr[spi][mag], 0, 0, surface_size, surface_size);
	cairo_fill (cache.cr[spi][mag]);
	
	/* star shadow */
	cairo_set_source_rgba(cache.cr[spi][mag], 0.0, 0.0, 0.0, 1.0);                                                             
    	cairo_arc (cache.cr[spi][mag], centre, centre, 
			radius * 1.1, 0, 2 * M_PI);
	cairo_fill (cache.cr[spi][mag]);
			
	/* star */
	cairo_set_source_rgba(cache.cr[spi][mag], 
				sp[spi].r, sp[spi].g, sp[spi].b, 
				star_sizes[mag].alpha);
   	cairo_arc(cache.cr[spi][mag], centre, centre, radius, 0, 2 * M_PI);
	cairo_fill(cache.cr[spi][mag]);

	return 0;
}

static gint render_cache(struct astrodb_table *table)
{
	gint spi, mag, ret;
	
	for (spi = 0; spi < SP_NUM; spi++) {
		for (mag = 0; mag < RENDER_MAG_BANDS; mag++) {
			ret = render_pixmap(spi, mag);
			if (ret < 0)
				return ret;
		}
	}
	return 0;
}

void star_object_free(struct astrodb_table *table)
{
	gint spi, mag;
	
	for (spi = 0; spi < SP_NUM; spi++) {
		for (mag = 0; mag < RENDER_MAG_BANDS; mag++) {
			cairo_destroy(cache.cr[spi][mag]);
		}
	}
}

gint star_object_init(struct astrodb_table *table)
{
	if (astrodb_table_register_schema(table, star_fields, 
					astrodb_size(star_fields),
					sizeof(struct sky2kv4_object)) < 0)
		g_critical("%s: failed to register object type\n", __func__);

	/* We want to quickly search the dataset based on object ID and HD number */
	if (astrodb_table_hash_key(table, "ID"))
		g_warning("%s: failed to hash on ID\n", __func__);
	if (astrodb_table_hash_key(table, "HD"))
		g_warning("%s: failed to hash on HD\n", __func__);
	if (astrodb_table_hash_key(table, "SAO"))
		g_warning("%s: failed to hash on SAO\n", __func__);
	if (astrodb_table_hash_key(table, "PPM"))
		g_warning("%s: failed to hash on PPM\n", __func__);
		
	if (astrodb_table_open(table, 20, 10, 20) < 0)
		g_critical("%s: table init failed\n", __func__);
	
	/* create pixmap cache for fast rendering */
	if (render_cache(table) < 0)
		g_critical("%s: failed to render cache\n", __func__);
	
	return 0;
}

static void sky2kv4_get_sp_index (struct sky2kv4_object *object, gchar *spect, 
	gint match)
{
	gint m = match;

	for (; m > 0; m--) {
		for (object->type = 0; object->type < SP_NUM; object->type++) {
			if (!strncmp(sp[object->type].sp, spect, m))
				return;
		 }
	}
}

void sky2kv4_sp_insert(void *dest, void *src)
{
	struct sky2kv4_object *so = 
		(struct sky2kv4_object*)((glong)dest -
		astrodb_offset(struct sky2kv4_object, sp));

	strcpy(dest, src);
	sky2kv4_get_sp_index(so, src, strlen(src));
}

