/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 


#include <libnova/neptune.h>
#include "planet.h"

static void neptune_update_cache(struct render_object *robject)
{
	gdouble JD = robject->context.JD;
	
	/* do we have to re-calc cache data */
	if (neptune.cache.epoch_JD + neptune.cache.diff_JD > JD ||
		neptune.cache.epoch_JD - neptune.cache.diff_JD < JD) {
		neptune.cache.epoch_JD = JD;
	
		neptune.cache.mag = neptune.get_mag(neptune.cache.epoch_JD);
		neptune.get_equ_posn(neptune.cache.epoch_JD, 
					&neptune.cache.posn);
	}
	robject->coord[0].object_size = 
		robject->context.faintest_magnitude - neptune.cache.mag;
	robject->coord[0].posn = &neptune.cache.posn;
	robject->object = &neptune;
}

static void neptune_render(struct render_object *robject, struct render_flags *rm)
{
	if (robject->type == RT_FULL) {

		if (rm->show_label) {
			gint label_offset = robject->coord[0].object_size + 15;
			cairo_set_source_rgb(robject->cr, 0.1, 0.4, 0.9); 
			cairo_move_to(robject->cr, 
					robject->coord[0].x - label_offset, 
					robject->coord[0].y + label_offset);
			cairo_show_text(robject->cr, neptune.sobject.name);
		}
		
		if (rm->show_axis_flag) {
	
		}
		
		cairo_set_source_rgb(robject->cr, 0.1, 0.4, 0.9);                                                             
		cairo_arc(robject->cr, 
				robject->coord[0].x, robject->coord[0].y, 
				robject->coord[0].object_size, 0, 2 * M_PI);
				
		if (!rm->show_outline) {
			cairo_fill (robject->cr);
		}
		
		if (rm->show_phase) {
		
		}
		
	} else {
		cairo_set_source_rgb(robject->cr, 0.1, 0.4, 0.9);                                                             
    		cairo_arc (robject->cr, 
    				robject->coord[0].x, robject->coord[0].y, 
    				robject->coord[0].object_size, 0, 2 * M_PI);
		cairo_fill (robject->cr);
	}
}

struct planet neptune = {
	.sobject.name = "Neptune",
	.render		= neptune_render,
	.update_cache   = neptune_update_cache,
	.get_equ_posn 	= ln_get_neptune_equ_coords,
	.get_earth_dist	= ln_get_neptune_earth_dist,
	.get_solar_dist	= ln_get_neptune_solar_dist, 
	.get_mag	= ln_get_neptune_magnitude,
	.get_disk	= ln_get_neptune_disk,
	.get_phase	= ln_get_neptune_phase,
	.get_equ_sdiam	= ln_get_neptune_sdiam,
	.get_pol_sdiam	= ln_get_neptune_sdiam,
	.get_rst	= ln_get_neptune_rst,
	.cache.epoch_JD = 0,
	.cache.diff_JD  = 0.1,
};
