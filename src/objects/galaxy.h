/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 

#ifndef __GALAXY_H
#define __GALAXY_H

#include "deep_object.h"

enum galaxy_type {
	GC_SP0 = 0,
	GC_IRR,
};

struct galaxy {
	struct astrodb_object dobject;
	enum galaxy_type type;		/*!< Galaxy type */
	gfloat maj_axis;		/*!< Galaxy major axis (arcmins) */
	gfloat min_axis;		/*!< Galaxy minor axis (arcmins) */
	gfloat pos_angle;		/*!< Galaxy position position_angle */
	gfloat radvel;			/*!< Galaxy radial velocity (kms) */
};

void galaxy_get_info(struct galaxy *object, GList *ex_info);

void galaxy_render(struct render_object *r);

#endif
