/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */


#ifndef __STAR_H
#define __STAR_H

#include "deep_object.h"

#define BRIGHEST_STAR	-2	/* brightest star we will render */
#define SP_NUM 180

struct sky2kv4_object {
	struct astrodb_object object;
	guchar type;
	gchar sp[3];		/* One dimensional SP class */
	gint HD;		/* HD number */
	gint SAO;		/* SAO number */
	gint PPM;		/* PPM number */
	gchar name[10];		/* Name or AGK3 number */
	gdouble pmRA;		/* proper motion in RA */
	gdouble pmDEC;		/* proper motion in DEC */
	gdouble RV;		/* Radial velocity */
	
	gdouble sep;		/* separation between 1st and 2nd brightest */
	gfloat Dmag;		/* mag difference */
	gdouble orbPer;		/* orbital period - years */
	gshort PA;		/* position angle */
	gdouble date;		/* observation date */
	gint ID_A;		/* primary component ID */
	gint ID_B;		/* primary component ID */
	gint ID_C;		/* primary component ID */
	
	gfloat magMax;		/* max variable mag */
	gfloat magMin;		/* min variable mag */
	gfloat varAmp;		/* variability magnitude */
	gdouble varPer;		/* period of variability - days */
	gdouble varEpo;		/* epoch of variability */
	gshort varType;		/* type of variable star */
};

#define sky2k4v_schema \
	astrodb_member("Name", "Name", struct sky2kv4_object, \
		name, CT_STRING, "", 0, NULL), \
	astrodb_member("ID", "ID", struct sky2kv4_object, \
		object.id, CT_STRING, "", 0, NULL), \
	astrodb_gmember("RA Hours", "RAh", struct sky2kv4_object, \
		object.posn_mag.ra,  CT_DOUBLE_HMS_HRS, "hours", 2, NULL), \
	astrodb_gmember("RA Minutes", "RAm", struct sky2kv4_object,\
		object.posn_mag.ra, CT_DOUBLE_HMS_MINS, "minutes", 1, NULL), \
	astrodb_gmember("RA Seconds", "RAs", struct sky2kv4_object, \
		object.posn_mag.ra, CT_DOUBLE_HMS_SECS, "seconds", 0, NULL), \
	astrodb_gmember("DEC Degrees", "DEd", struct sky2kv4_object, \
		object.posn_mag.dec, CT_DOUBLE_DMS_DEGS, "degrees", 3, NULL), \
	astrodb_gmember("DEC Minutes", "DEm", struct sky2kv4_object, \
		object.posn_mag.dec, CT_DOUBLE_DMS_MINS, "minutes", 2, NULL), \
	astrodb_gmember("DEC Seconds", "DEs", struct sky2kv4_object, \
		object.posn_mag.dec, CT_DOUBLE_DMS_SECS, "seconds", 1, NULL), \
	astrodb_gmember("DEC sign", "DE-", struct sky2kv4_object, \
		object.posn_mag.dec, CT_SIGN, "", 0, NULL), \
	astrodb_member("Visual Mag", "Vmag", struct sky2kv4_object, \
		object.posn_mag.Vmag, CT_FLOAT, "", 0, NULL), \
	astrodb_member("sp", "Sp", struct sky2kv4_object, \
		sp, CT_STRING, "", 0, sky2kv4_sp_insert), \
	astrodb_member("HD", "HD", struct sky2kv4_object, \
		HD, CT_INT, "", 0, NULL), \
	astrodb_member("SAO", "SAO", struct sky2kv4_object, \
		SAO, CT_INT, "", 0, NULL),\
	astrodb_member("PPM", "PPM", struct sky2kv4_object, \
		PPM, CT_INT, "", 0, NULL),\
	astrodb_member("pmRA", "pmRA", struct sky2kv4_object, \
		pmRA, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("pmDEC", "pmDEC", struct sky2kv4_object, \
		pmDEC, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("Radial Vel", "RV", struct sky2kv4_object, \
		RV, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("Binary sep", "sep", struct sky2kv4_object, \
		sep, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("Dmag", "Dmag", struct sky2kv4_object, \
		Dmag, CT_FLOAT, "", 0, NULL),\
	astrodb_member("Orb Per", "orbPer", struct sky2kv4_object, \
		orbPer, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("Pos Angle", "PA", struct sky2kv4_object, \
		PA, CT_SHORT, "", 0, NULL),\
	astrodb_member("Obs Date", "date", struct sky2kv4_object, \
		date, CT_DOUBLE, "", 0, NULL),\
	astrodb_member("ID_A", "ID_A", struct sky2kv4_object, \
		ID_A, CT_INT, "", 0, NULL),\
	astrodb_member("ID_B", "ID_B", struct sky2kv4_object, \
		ID_B, CT_INT, "", 0, NULL),\
	astrodb_member("ID_C", "ID_C", struct sky2kv4_object, \
		ID_C, CT_INT, "", 0, NULL), \
	astrodb_member("Var max mag", "magMax", struct sky2kv4_object, \
		magMax, CT_FLOAT, "", 0, NULL), \
	astrodb_member("Var min mag", "magMin", struct sky2kv4_object, \
		magMin, CT_FLOAT, "", 0, NULL), \
	astrodb_member("Var Amp", "varAmp", struct sky2kv4_object, \
		varAmp, CT_FLOAT, "", 0, NULL), \
	astrodb_member("Var Period", "varPer", struct sky2kv4_object, \
		varPer, CT_DOUBLE, "", 0, NULL), \
	astrodb_member("Var Epoch", "varEpo", struct sky2kv4_object, \
		varEpo, CT_DOUBLE, "", 0, NULL), \
	astrodb_member("Var Type", "varType", struct sky2kv4_object, \
		varType, CT_SHORT, "", 0, NULL),

gint star_object_init(struct astrodb_table *table);
void star_object_render(struct render_object *r);
void star_object_free(struct astrodb_table *table);

void sky2kv4_sp_insert(void *dest, void *src);

#endif
