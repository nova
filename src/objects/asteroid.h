/*
 * Copyright (C) 2005 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __ASTEROID_H
#define __ASTEROID_H

#include "solar_object.h"

#define AST_COMP_SIZE	10
#define AST_DES_SIZE	28

struct asteroid {
	struct solar_object nobject;
	struct ln_ell_orbit orbit;
	gfloat H;
	gfloat A;
	gdouble G;
	gchar computer[AST_COMP_SIZE];
	gchar designation[AST_DES_SIZE];
};


void asteroid_get_equ_posn(struct asteroid *object, gdouble JD,
				struct ln_equ_posn* posn);

void asteroid_get_hrz_posn (struct asteroid *object, gdouble JD, 
				struct ln_lnlat_posn* observer, 
				struct ln_hrz_posn* posn);

void asteroid_get_rst_time (struct asteroid *object, gdouble JD, 
				struct ln_lnlat_posn* observer, 
				struct ln_rst_time* time);

double asteroid_get_mag(struct asteroid *object, gdouble JD);

void asteroid_render(struct render_object* r);
 
gdouble asteroid_get_earth_dist (struct asteroid *object, gdouble JD);
gdouble asteroid_get_sun_dist (struct asteroid *object, gdouble JD);

gdouble asteroid_get_sdiam_km (struct asteroid * object);
gdouble asteroid_get_sdiam_arc (struct asteroid * object, gdouble JD);
		
#endif
