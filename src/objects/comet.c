/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include "comet.h"

#include <libnova/utility.h>
#include <libnova/transform.h>
#include <libnova/rise_set.h>
#include <libnova/comet.h>
#include <libnova/elliptic_motion.h>
#include <libnova/parabolic_motion.h>
#include <libnova/hyperbolic_motion.h>
#include <libnova/julian_day.h>

struct orbit_ops {
	void (*set_ecc)(struct comet *comet, gdouble e);
	void (*set_pds)(struct comet *comet, gdouble p);
	void (*set_per)(struct comet *comet, gdouble p);
	void (*set_lan)(struct comet *comet, gdouble l);
	void (*set_inc)(struct comet *comet, gdouble i);
	void (*set_eph)(struct comet *comet, gdouble e);

};

static struct orbit_ops *comet;

static inline void ell_set_ecc (struct comet *comet, gdouble e) 
{comet->orbit.e.e = e;}
static inline void hyp_set_ecc (struct comet *comet, gdouble e) 
{comet->orbit.h.e = e;}
static inline void par_set_ecc (struct comet *comet, gdouble e) 
{}
static inline void ell_set_pds (struct comet *comet, gdouble p) 
{comet->orbit.e.a = p / comet->orbit.e.e;}
static inline void hyp_set_pds (struct comet *comet, gdouble p) 
{comet->orbit.h.q = p;}
static inline void par_set_pds (struct comet *comet, gdouble p) 
{comet->orbit.p.q = p;}
static inline void ell_set_per (struct comet *comet, gdouble p) 
{comet->orbit.e.w = p;}
static inline void hyp_set_per (struct comet *comet, gdouble p) 
{comet->orbit.h.w = p;}
static inline void par_set_per (struct comet *comet, gdouble p) 
{comet->orbit.p.w = p;}
static inline void ell_set_lan (struct comet *comet, gdouble l) 
{comet->orbit.e.omega = l;}
static inline void hyp_set_lan (struct comet *comet, gdouble l) 
{comet->orbit.h.omega = l;}
static inline void par_set_lan (struct comet *comet, gdouble l) 
{comet->orbit.p.omega = l;}
static inline void ell_set_inc (struct comet *comet, gdouble i)
{comet->orbit.e.i = i;}
static inline void hyp_set_inc (struct comet *comet, gdouble i) 
{comet->orbit.h.i = i;}
static inline void par_set_inc (struct comet *comet, gdouble i) 
{comet->orbit.p.i = i;}
static inline void ell_set_eph (struct comet *comet, gdouble e) 
{comet->orbit.e.JD = e;}
static inline void hyp_set_eph (struct comet *comet, gdouble e) 
{comet->orbit.h.JD = e;}
static inline void par_set_eph (struct comet *comet, gdouble e) 
{comet->orbit.p.JD = e;}

/* eccentricity 0...1 */
static struct orbit_ops elliptic = {
	.set_ecc = ell_set_ecc,
	.set_pds = ell_set_pds,
	.set_per = ell_set_per,
	.set_lan = ell_set_lan,
	.set_inc = ell_set_inc,
	.set_eph = ell_set_eph,
};

/* eccentricity > 1 */
static struct orbit_ops hyperbolic = {
	.set_ecc = hyp_set_ecc,
	.set_pds = hyp_set_pds,
	.set_per = hyp_set_per,
	.set_lan = hyp_set_lan,
	.set_inc = hyp_set_inc,
	.set_eph = hyp_set_eph,
};

/* eccentricity == 1*/
static struct orbit_ops parabolic = {
	.set_ecc = par_set_ecc,
	.set_pds = par_set_pds,
	.set_per = par_set_per,
	.set_lan = par_set_lan,
	.set_inc = par_set_inc,
	.set_eph = par_set_eph,
};

static struct ln_date date;

void comet_get_equ_posn (struct comet *comet, gdouble JD, 
				struct ln_equ_posn *posn)
{	
	switch (comet->type) {
	case CT_PARABOLIC:
		ln_get_par_body_equ_coords (JD, &comet->orbit.p, posn);
		break;
	case CT_ELLIPTIC:
		ln_get_ell_body_equ_coords (JD, &comet->orbit.e, posn);
		break;
	case CT_HYPERBOLIC:
		ln_get_hyp_body_equ_coords (JD, &comet->orbit.h, posn);
		break;
	}
}

void comet_get_hrz_posn (struct comet *comet, gdouble JD, 
			 struct ln_lnlat_posn *observer, 
			 struct ln_hrz_posn *posn)
{
	struct ln_equ_posn equ_posn;
		
	comet_get_equ_posn(comet, JD, &equ_posn);
	ln_get_hrz_from_equ (&equ_posn, observer, JD, posn);
}

void comet_get_rst_time (struct comet *comet, gdouble JD, 
				struct ln_lnlat_posn *observer, 
				struct ln_rst_time *time)
{
	switch (comet->type) {
	case CT_PARABOLIC:
		ln_get_par_body_rst(JD, observer, &comet->orbit.p, time);
		break;
	case CT_ELLIPTIC:
		ln_get_ell_body_rst(JD, observer, &comet->orbit.e, time);
		break;
	case CT_HYPERBOLIC:
		ln_get_hyp_body_rst(JD, observer, &comet->orbit.h, time);
		break;
	}
}

gdouble comet_get_mag(struct comet *comet, gdouble JD)
{
	gdouble mag = 0;
	
	switch (comet->type) {
	case CT_PARABOLIC:
		mag = ln_get_par_comet_mag(JD, &comet->orbit.p,
						comet->g, comet->k);
		break;
	case CT_ELLIPTIC:
		mag = ln_get_ell_comet_mag(JD, &comet->orbit.e, 
						comet->g, comet->k);
		break;
	case CT_HYPERBOLIC:
	/*	mag = ln_get_hyp_comet_mag(JD, &comet->orbit.h, 
						comet->g, comet->k); */
		break;
	}
	return mag;
}

void comet_render(struct render_object* r)
{

}

gdouble comet_get_earth_dist (struct comet *comet, gdouble JD)
{
	gdouble au = 0;
	
	switch (comet->type) {
	case CT_PARABOLIC:
		au = ln_get_par_body_earth_dist(JD, &comet->orbit.p);
		break;
	case CT_ELLIPTIC:
		au = ln_get_ell_body_earth_dist(JD, &comet->orbit.e);
		break;
	case CT_HYPERBOLIC:
		au = ln_get_hyp_body_earth_dist(JD, &comet->orbit.h);
		break;
	}
	return au;
}

gdouble comet_get_sun_dist (struct comet *comet, gdouble JD)
{
	gdouble au = 0;
	
	switch (comet->type) {
	case CT_PARABOLIC:
		au = ln_get_par_body_solar_dist(JD, &comet->orbit.p);
		break;
	case CT_ELLIPTIC:
		au = ln_get_ell_body_solar_dist(JD, &comet->orbit.e);
		break;
	case CT_HYPERBOLIC:
		au = ln_get_hyp_body_solar_dist(JD, &comet->orbit.h);
		break;
	}
	return au;
}
