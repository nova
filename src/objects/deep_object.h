/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __DEEP_OBJECT_H
#define __DEEP_OBJECT_H

#include <libnova/ln_types.h>
#include "astro_object.h"

void deep_object_get_hrz_posn (struct astrodb_object *object, gdouble JD, 
	struct ln_lnlat_posn *observer, struct ln_hrz_posn *posn);

void deep_object_get_rst_time (struct astrodb_object *object, gdouble JD, 
	struct ln_lnlat_posn *observer, struct ln_rst_time *time);

void deep_object_get_info (struct astrodb_object *object, GList *value);

void deep_object_render(struct render_object *r);

#endif
