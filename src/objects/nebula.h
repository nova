/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 

#ifndef __NEBULA_H
#define __NEBULA_H

#include "deep_object.h"

struct nebula {
	struct astrodb_object dobject;
	gfloat maj_axis;		/*!< major axis (arcmins) */
	gfloat min_axis;		/*!< minor axis (arcmins) */
	gfloat pos_angle;		/*!< position position_angle */
	gfloat radvel;			/*!< radial velocity (kms) */
};

void nebula_get_info(struct nebula *object, GList *ex_info);

void nebula_render(struct render_object *r);

#endif
