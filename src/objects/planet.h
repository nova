/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __PLANET_H
#define __PLANET_H

#include <libnova/ln_types.h>

#include "solar_object.h"
#include "render.h"

struct planet {
	struct solar_object sobject;
	gchar name[12];
	void (*render)(struct render_object *r, struct render_flags* rm);
	void (*update_cache)(struct render_object *r);
	/* libnova interface */
	void (*get_equ_posn) (gdouble JD, struct ln_equ_posn* posn);
	gdouble (*get_earth_dist)(gdouble JD);
	gdouble (*get_solar_dist)(gdouble JD);
	gdouble (*get_mag)(gdouble JD);
	gdouble (*get_disk)(gdouble JD);
	gdouble (*get_phase)(gdouble JD);
	gdouble (*get_equ_sdiam)(gdouble JD);
	gdouble (*get_pol_sdiam)(gdouble JD);
	int (*get_rst)(gdouble JD, struct ln_lnlat_posn * observer, 
		struct ln_rst_time * rst);
	struct solar_object_cache cache;
};

extern struct planet mercury;
extern struct planet venus;
extern struct planet mars;
extern struct planet jupiter;
extern struct planet saturn;
extern struct planet uranus;
extern struct planet neptune;
extern struct planet pluto;

#endif
