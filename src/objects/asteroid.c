/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include "asteroid.h"

#include <libnova/utility.h>
#include <libnova/transform.h>
#include <libnova/rise_set.h>
#include <libnova/asteroid.h>
#include <libnova/elliptic_motion.h>
#include <libnova/julian_day.h>

void asteroid_get_equ_posn (struct asteroid * object, gdouble JD,
				struct ln_equ_posn* posn)
{
	ln_get_ell_body_equ_coords (JD, &object->orbit, posn);
}

void asteroid_get_hrz_posn (struct asteroid * object, gdouble JD, 
				struct ln_lnlat_posn* observer, 
				struct ln_hrz_posn* posn)
{
	struct ln_equ_posn equ_posn;
	asteroid_get_equ_posn(object, JD, &equ_posn);
	ln_get_hrz_from_equ (&equ_posn, observer, JD, posn);
}

void asteroid_get_rst_time (struct asteroid * object, gdouble JD, 
				struct ln_lnlat_posn* observer, 
				struct ln_rst_time* time)
{
	ln_get_ell_body_rst(JD, observer, &object->orbit, time);
}

gdouble asteroid_get_mag(struct asteroid * object, gdouble JD)
{
	return ln_get_asteroid_mag (JD, &object->orbit, object->H, object->G);
}

void asteroid_render(struct render_object* r)
{
}

gdouble asteroid_get_earth_dist (struct asteroid * object, gdouble JD)
{
	return ln_get_ell_body_earth_dist (JD, &object->orbit);
}

gdouble asteroid_get_sun_dist (struct asteroid * object, gdouble JD)
{
	return ln_get_ell_body_solar_dist (JD, &object->orbit);
}

gdouble asteroid_get_sdiam_km (struct asteroid * object)
{
	return ln_get_asteroid_sdiam_km(object->H, object->A);
}

gdouble asteroid_get_sdiam_arc (struct asteroid * object, gdouble JD)
{
	return ln_get_asteroid_sdiam_arc(JD, &object->orbit,
						object->H, object->A);
}
