/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __ASTRO_OBJECT_H
#define __ASTRO_OBJECT_H

#include <libastrodb/astrodb.h>
#include <glib-2.0/glib.h>
#include "render.h"

#define noffset(x,y) (long)(&((x*)0)->y) 	/* offset in struct */
#define nsizeof(x,y) sizeof(((x*)0)->y) 	/* size in struct */
#define nsize(x) (sizeof(x)/sizeof(x[0]))	/* array size */

enum object_type
{
	OT_STAR = 0,
	OT_SOLAR,
	OT_LUNAR,
	OT_PLANET,
};

struct astro_object {
	gchar  *type;
	gint (*init)(struct astrodb_table *table);
	void (*free)(struct astrodb_table *table);
	void (*render)(struct render_object *r);
	gint (*info)(void *object, GList *info);
};

#endif
