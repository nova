/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 

#ifndef __COMET_H
#define __COMET_H

#include "solar_object.h"

#define COMET_DES_SIZE	28

enum comet_type {
	CT_ELLIPTIC,
	CT_PARABOLIC,
	CT_HYPERBOLIC
};

struct comet {
	struct solar_object nobject;
	union {
		struct ln_ell_orbit e;
		struct ln_par_orbit p;
		struct ln_hyp_orbit h;
	} orbit;
	gdouble g;
	gdouble k;
	enum comet_type type;
	gchar designation[COMET_DES_SIZE];
};

void comet_get_equ_posn(struct comet *comet, gdouble JD, 
			struct ln_equ_posn *posn);

void comet_get_hrz_posn (struct comet *comet, gdouble JD, 
				struct ln_lnlat_posn *observer, 
				struct ln_hrz_posn *posn);

void comet_get_rst_time (struct comet *comet, gdouble JD, 
				struct ln_lnlat_posn *observer, 
				struct ln_rst_time *time);

gdouble comet_get_mag(struct comet *comet, gdouble JD);
 
gdouble comet_get_earth_dist (struct comet *comet, gdouble JD);
	
gdouble comet_get_sun_dist (struct comet *comet, gdouble JD);

void comet_render(struct render_object *r);

#endif
