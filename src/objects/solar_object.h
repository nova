/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __SOLAR_OBJECT_H
#define __SOLAR_OBJECT_H

#include <libnova/ln_types.h>
#include "astro_object.h"

#define SOLAR_NAME_SIZE		16

struct solar_object_cache {
	gdouble epoch_JD;	/* JD when properties were last calculated */
	gdouble diff_JD;	/* If current JD > epoch + diff then recalc properties */
	struct ln_equ_posn posn;
	gdouble earth_dist;
	gdouble solar_dist;
	gdouble mag;
	gdouble disk;
	gdouble phase;
	gdouble equ_sdiam;
	gdouble pol_sdiam;
	struct ln_rst_time rst;
};

struct solar_object {
	struct astrodb_object aobject;
	gfloat AU;
	gchar name[SOLAR_NAME_SIZE];
};

#endif
