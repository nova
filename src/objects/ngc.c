/*
 * Copyright (C) 2008 Liam Girdwood
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */ 

#include <errno.h>
#include <string.h>
#include <math.h>
#include <cairo.h>
#include "ngc.h"

struct ngc_pixmap_cache {
	cairo_t *cr[NGC_TYPES][RENDER_MAG_BANDS];
	cairo_surface_t *surface[NGC_TYPES][RENDER_MAG_BANDS];
	gint offset[NGC_TYPES][RENDER_MAG_BANDS];
	gdouble object_size[NGC_TYPES][RENDER_MAG_BANDS];
};
struct ngc_render_size {
	gdouble size;
	gdouble alpha;
};

static struct ngc_pixmap_cache cache;

const gchar *ngc_type[NGC_TYPES][2] = {
    { " Gx",	"Galaxy"},
    { " OC",	"Open star cluster"},
    { " Gb",	"Globular star cluster, usually in the Milky Way Galaxy"},
    { " Nb",	"Bright emission or reflection nebula"},
    { " Pl",	"Planetary nebula"},
    { "C+N",	"Cluster associated with nebulosity"}, 
    { "Ast",	"Asterism or group of a few stars"}, 
    { " Kt",	"Knot  or  nebulous  region  in  an  external galaxy"},
    { "***",	"Triple star"},
    { " D*",	"Double star"},
    { "  *",	"Single star"},
    { "  ?",	"Uncertain type or may not exist"},
    { "   ",	"Unidentified at the place given, or type unknown"},
    { "  -",	"Object called nonexistent in the RNGC (Sulentic and Tifft 1973)"},
    { " PD",	"Photographic plate defect"},
};

static struct astrodb_schema_object ngc_fields[] = {
	#if 0
	astrodb_member("Name", "Name", struct ngc_object, dobject.aobject.name,
		   CT_STRING, "", 0, NULL),
	astrodb_member("Type", "Type", struct ngc_object, dobject.aobject.id,
		   CT_STRING, "", 0, NULL),
	astrodb_member("RA", "RA", struct ngc_object, dobject.posn.ra, 
		   CT_DOUBLE, "degrees", 0, NULL),
	astrodb_member("DEC", "DEC", struct ngc_object, dobject.posn.dec, 
		   CT_DOUBLE, "degrees", 0, NULL),
	astrodb_gmember("RA Hours", "RAh", struct ngc_object, dobject.posn.ra, 
		    CT_DOUBLE_HMS_HRS, "hours", 2, NULL),
	astrodb_gmember("RA Minutes", "RAm", struct ngc_object, dobject.posn.ra, 
		    CT_DOUBLE_HMS_MINS, "minutes", 1, NULL),
	astrodb_gmember("RA Seconds", "RAs", struct ngc_object, dobject.posn.ra, 
		    CT_DOUBLE_HMS_SECS, "seconds", 0, NULL),
	astrodb_gmember("DEC Degrees", "DEd", struct ngc_object, dobject.posn.dec, 
		    CT_DOUBLE_DMS_DEGS, "degrees", 3, NULL),
	astrodb_gmember("DEC Minutes", "DEm", struct ngc_object, dobject.posn.dec, 
		    CT_DOUBLE_DMS_MINS, "minutes", 2, NULL),
	astrodb_gmember("DEC Seconds", "DEs", struct ngc_object, dobject.posn.dec, 
		    CT_DOUBLE_DMS_SECS, "seconds", 1, NULL),
	astrodb_gmember("DEC sign", "DE-", struct ngc_object, dobject.posn.dec, 
		    CT_SIGN, "", 0, NULL),
	astrodb_member("Visual Mag", "mag", struct ngc_object, dobject.Vmag, 
		   CT_FLOAT, "", 0, NULL),
#endif
	astrodb_member("Size", "size", struct ngc_object, axis,
		   CT_FLOAT, "", 0, NULL),
};


void ngc_object_render(struct render_object *robject)
{
	struct ngc_object *object = (struct ngc_object*)robject->object;
//	guint i = object->type;
	gint size;
#if 0
	size = 2 + ceil(
		(robject->context.faintest_magnitude - object->dobject.Vmag) 
		* 2.5);

	if (size >= RENDER_MAG_BANDS)
		size = RENDER_MAG_BANDS - 1;
	if (size < 0)
		size = 0;
	if (i >= NGC_TYPES)
		i = NGC_TYPES - 1;

	robject->coord[0].object_size = cache.object_size[i][size];

	cairo_set_source_surface(robject->cr, cache.surface[i][size], 
				robject->coord[0].x - cache.offset[i][size], 
				robject->coord[0].y - cache.offset[i][size]);
	cairo_paint(robject->cr);
#endif
}

static gint render_pixmap(gint spi, gint mag)
{
#if 0
	gint size = ngc_sizes[mag].size;
	gint surface_size = size + 3;
	gdouble radius = (gdouble)size / 2.0;
	gdouble centre = ((gdouble)size / 2.0) + 1.5;
	
	cache.surface[spi][mag] = 
		cairo_image_surface_create(CAIRO_FORMAT_ARGB32, surface_size,
						surface_size);
			
	if (cache.surface[spi][mag] == NULL)
		return -ENOMEM;
			
	cache.cr[spi][mag] = cairo_create(cache.surface[spi][mag]);
	if (cache.cr[spi][mag] == NULL)
		return -ENOMEM;
	
	cache.offset[spi][mag] = centre;
	cache.object_size[spi][mag] = radius * 2.0;

	/* render_object transparent background */
	cairo_set_source_rgba(cache.cr[spi][mag], 0.0, 0.0, 0.0, 0.0);  
	cairo_rectangle(cache.cr[spi][mag], 0, 0, surface_size, surface_size);
	cairo_fill (cache.cr[spi][mag]);
	
	/* star shadow */
	cairo_set_source_rgba(cache.cr[spi][mag], 0.0, 0.0, 0.0, 1.0);                                                             
    	cairo_arc (cache.cr[spi][mag], centre, centre, 
			radius * 1.1, 0, 2 * M_PI);
	cairo_fill (cache.cr[spi][mag]);
			
	/* star */
	cairo_set_source_rgba(cache.cr[spi][mag], 
				sp[spi].r, sp[spi].g, sp[spi].b, 
				ngc_sizes[mag].alpha);
   	cairo_arc(cache.cr[spi][mag], centre, centre, radius, 0, 2 * M_PI);
	cairo_fill(cache.cr[spi][mag]);
#endif
	return 0;
}

static gint render_cache(struct astrodb_table *table)
{
	gint spi, mag, ret;
	
	for (spi = 0; spi < NGC_TYPES; spi++) {
		for (mag = 0; mag < RENDER_MAG_BANDS; mag++) {
			ret = render_pixmap(spi, mag);
			if (ret < 0)
				return ret;
		}
	}
	return 0;
}

void ngc_object_free(struct astrodb_table *table)
{
	gint spi, mag;
	
	for (spi = 0; spi < NGC_TYPES; spi++) {
		for (mag = 0; mag < RENDER_MAG_BANDS; mag++) {
			cairo_destroy(cache.cr[spi][mag]);
		}
	}
}

gint ngc_object_init(struct astrodb_table *table)
{	
	if (astrodb_table_register_schema(table, ngc_fields, 
					astrodb_size(ngc_fields),
					sizeof(struct ngc_object)) < 0)
		g_critical("%s: failed to register object type\n", __func__);

	/* We want to quickly search the dataset based on object name */
	if (astrodb_table_hash_key(table, "Name"))
		g_warning("%s: failed to hash on Name\n", __func__);

	if (astrodb_table_open(table, 90, 45, 20) < 0)
		g_critical("%s: table init failed\n", __func__);
	
	/* create pixmap cache for fast rendering */
	if (render_cache(table) < 0)
		g_critical("%s: failed to render cache\n", __func__);
	
	return 0;
}
